package gov.mil.esp.proceso.domain;

import org.springframework.http.HttpStatus;

import lombok.Data;

@Data
public class Response <T>{	
	
	private String codigo;
	private String mensaje;
	private T payload;

}
	