package gov.mil.esp.proceso.domain.business;

import org.postgresql.util.PSQLException;

import gov.mil.esp.proceso.domain.dto.CrearDocumentoDTO;
import gov.mil.esp.proceso.domain.exception.DocumentoNoexisteException;
import gov.mil.esp.proceso.domain.exception.ProcesoNoExisteException;
import gov.mil.esp.proceso.model.DocumentoProceso;

public interface DocumentoProcesoBusiness {

	public void uploadDocument(CrearDocumentoDTO crearDocumentoDTO) throws PSQLException, ProcesoNoExisteException;

	public DocumentoProceso getDocumento(String nombreProceso, String nombreDocumento)
			throws DocumentoNoexisteException;

}
