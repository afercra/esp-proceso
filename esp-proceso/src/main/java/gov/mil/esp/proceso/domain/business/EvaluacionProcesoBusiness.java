package gov.mil.esp.proceso.domain.business;

import java.util.List;

import org.springframework.stereotype.Service;

import gov.mil.esp.proceso.domain.dto.EvaluacionCriterioCalificacionProcesoDTO;
import gov.mil.esp.proceso.domain.dto.EvaluacionCriterioCalificacionProcesoGridDTO;

@Service
public interface EvaluacionProcesoBusiness {
	public String saveEvaluacionCriterios(EvaluacionCriterioCalificacionProcesoDTO request);

	List<EvaluacionCriterioCalificacionProcesoGridDTO> getEvaluacionCriteriosCalificacionProceso(Integer numeroProceso);
	
	
}
