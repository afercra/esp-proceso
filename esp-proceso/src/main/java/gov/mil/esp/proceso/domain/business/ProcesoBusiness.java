package gov.mil.esp.proceso.domain.business;

import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;

import gov.mil.esp.proceso.domain.dto.AsignarProcesoAEntrevistadorDTO;
import gov.mil.esp.proceso.domain.dto.CambioDecisionProcesoDTO;
import gov.mil.esp.proceso.domain.exception.EntrevistadorNoExisteException;
import gov.mil.esp.proceso.domain.exception.ProcesoEnCursoException;
import gov.mil.esp.proceso.domain.exception.ProcesoNoAsignableException;
import gov.mil.esp.proceso.domain.exception.ProcesoNoAsignadoException;
import gov.mil.esp.proceso.model.Proceso;

public interface ProcesoBusiness {

	
	public Proceso iniciarProceso(String documentoIdentidad) throws NoSuchElementException, ProcesoEnCursoException;

	public void getEstadosProceso();

	public void getDecisionesProceso();

	public HashMap<String, String> getParametrosProceso();

	public String estructurarNumeroProceso(String numeroDocumento);

	public List<Proceso> cambiarDecisionProceso(List<CambioDecisionProcesoDTO> procesos);

	public void asignarProcesoAEntrevistador(AsignarProcesoAEntrevistadorDTO asignarProcesoAEntrevistadorDTO)
			throws ProcesoNoAsignableException, EntrevistadorNoExisteException;

	public void desasignarProcesoAEntrevistador(AsignarProcesoAEntrevistadorDTO desasignarProcesoAEntrevistadorDTO)
			throws EntrevistadorNoExisteException, ProcesoNoAsignadoException;
}
