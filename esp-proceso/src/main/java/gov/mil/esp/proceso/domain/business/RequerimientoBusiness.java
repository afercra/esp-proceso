package gov.mil.esp.proceso.domain.business;

import java.util.List;

import gov.mil.esp.proceso.domain.dto.CambiarEstadoRequerimientoDTO;
import gov.mil.esp.proceso.domain.dto.CreacionRequerimientoDTO;
import gov.mil.esp.proceso.model.Requerimiento;

public interface RequerimientoBusiness {

	public Requerimiento crearRequerimiento(List<CreacionRequerimientoDTO> procesoRequest) ;

	public void eliminarRequerimiento(String nombreRequerimiento);

	public List<Requerimiento> cambiarEstadoRequerimiento(CambiarEstadoRequerimientoDTO requerimientosRequest) ;
	
}
