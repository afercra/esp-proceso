package gov.mil.esp.proceso.domain.business.impl;

import org.postgresql.util.PSQLException;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import gov.mil.esp.proceso.domain.business.DocumentoProcesoBusiness;
import gov.mil.esp.proceso.domain.dto.CrearDocumentoDTO;
import gov.mil.esp.proceso.domain.exception.DocumentoNoexisteException;
import gov.mil.esp.proceso.domain.exception.ProcesoNoExisteException;
import gov.mil.esp.proceso.model.DocumentoProceso;
import gov.mil.esp.proceso.persistence.DocumentoProcesoRepository;
import gov.mil.esp.proceso.persistence.ProcesoRepository;
import gov.mil.esp.proceso.persistence.entity.DocumentoProcesoPersistenceEntity;

@Component
public class DocumentoProcesoBusinessImpl implements DocumentoProcesoBusiness {

	private DocumentoProcesoRepository documentoProcesoRepository;
	private ProcesoRepository procesoRepository;

	public DocumentoProcesoBusinessImpl(DocumentoProcesoRepository documentoProcesoRepository,
			ProcesoRepository procesoRepository) {
		this.documentoProcesoRepository = documentoProcesoRepository;
		this.procesoRepository = procesoRepository;
	}

	public void uploadDocument(CrearDocumentoDTO crearDocumentoDTO) throws PSQLException, ProcesoNoExisteException {
		System.out.println("crearDocumentoDTO");
		System.out.println(crearDocumentoDTO);
		System.out.println(crearDocumentoDTO.getNumeroProceso());

		procesoRepository.findById(crearDocumentoDTO.getNumeroProceso()).orElseThrow(
				() -> new ProcesoNoExisteException("No existe el proceso al que se le quiere asignar el documento"));

		DocumentoProcesoPersistenceEntity documentoProcesoPersistenceEntity = documentoProcesoRepository
				.getDocumentoByNameAndNumeroProceso(crearDocumentoDTO.getNumeroProceso(),
						crearDocumentoDTO.getNombreDocumento());

		System.out.println("Documento Persistence Entity is nulls");
		System.out.println(documentoProcesoPersistenceEntity == null);

		Integer id = documentoProcesoRepository.getLastProcessId();

		if (documentoProcesoPersistenceEntity == null) {
			documentoProcesoPersistenceEntity = new DocumentoProcesoPersistenceEntity();

			if (id == null) {
				documentoProcesoPersistenceEntity.setIdDocumento(1);
			} else {
				documentoProcesoPersistenceEntity.setIdDocumento(id + 1);
			}
		}

		BeanUtils.copyProperties(crearDocumentoDTO, documentoProcesoPersistenceEntity);

		documentoProcesoRepository.saveAndFlush(documentoProcesoPersistenceEntity);
	}

	public DocumentoProceso getDocumento(String nombreProceso, String nombreDocumento)
			throws DocumentoNoexisteException {
		DocumentoProcesoPersistenceEntity documentoProcesoPersistenceEntity = documentoProcesoRepository
				.getDocumentoByNameAndProceso(nombreProceso, nombreDocumento);

		if (documentoProcesoPersistenceEntity == null) {
			throw new DocumentoNoexisteException(
					"El documento:" + nombreDocumento + " no existe para el proceso: " + nombreProceso);
		}

		DocumentoProceso documentoProceso = new DocumentoProceso();
		BeanUtils.copyProperties(documentoProcesoPersistenceEntity, documentoProceso);
		System.out.println(documentoProceso);
		return documentoProceso;
	}

}
