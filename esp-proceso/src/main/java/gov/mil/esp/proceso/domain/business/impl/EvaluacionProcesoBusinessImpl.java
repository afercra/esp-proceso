package gov.mil.esp.proceso.domain.business.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import gov.mil.esp.proceso.domain.Response;
import gov.mil.esp.proceso.domain.business.EvaluacionProcesoBusiness;
import gov.mil.esp.proceso.domain.dto.CriterioCalificacionProcesoDTO;
import gov.mil.esp.proceso.domain.dto.EvaluacionCriterioCalificacionProcesoDTO;
import gov.mil.esp.proceso.domain.dto.EvaluacionCriterioCalificacionProcesoGridDTO;
import gov.mil.esp.proceso.persistence.CriterioCalificacionProcesoRepository;
import gov.mil.esp.proceso.persistence.EvaluacionCriterioCalificacionProcesoFacadeRepository;
import gov.mil.esp.proceso.persistence.EvaluacionCriterioCalificacionProcesoRepository;
import gov.mil.esp.proceso.persistence.ProcesoRepository;
import gov.mil.esp.proceso.persistence.entity.EvaluacionCriterioCalificacionProcesoDTOEntity;

@Service
public class EvaluacionProcesoBusinessImpl implements EvaluacionProcesoBusiness {

	private EvaluacionCriterioCalificacionProcesoFacadeRepository evaluacionCriterioCalificacionProcesoFacadeRepository;
	private EvaluacionCriterioCalificacionProcesoRepository evaluacionCriterioCalificacionRepository;
	private CriterioCalificacionProcesoRepository criterioCalificacionProcesoRepository;
	private ProcesoRepository procesoRepository;

	@Autowired
	public EvaluacionProcesoBusinessImpl(
			EvaluacionCriterioCalificacionProcesoRepository evaluacionCriterioCalificacionRepository,
			CriterioCalificacionProcesoRepository criterioCalificacionProcesoRepository,
			EvaluacionCriterioCalificacionProcesoFacadeRepository evaluacionCriterioCalificacionProcesoFacadeRepository,
			ProcesoRepository procesoRepository) {
		this.evaluacionCriterioCalificacionRepository = evaluacionCriterioCalificacionRepository;
		this.criterioCalificacionProcesoRepository = criterioCalificacionProcesoRepository;
		this.evaluacionCriterioCalificacionProcesoFacadeRepository = evaluacionCriterioCalificacionProcesoFacadeRepository;
		this.procesoRepository = procesoRepository;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Integer getLastEvaluacionCriterioCalificacionProcesoDTOId() {
		return evaluacionCriterioCalificacionRepository.getLastEvaluacionCriterioCalificacionProcesoRepositoryId();
	}

	@Override
	@Transactional
	public String saveEvaluacionCriterios(EvaluacionCriterioCalificacionProcesoDTO request) {

		Integer proceso = request.getProceso();
		List<CriterioCalificacionProcesoDTO> criteriosCalificacionList = request.getCriterioCalificacionList();

		Long longId = evaluacionCriterioCalificacionRepository.count();
		Integer id = longId.intValue();

		for (CriterioCalificacionProcesoDTO criterioCalificacionProcesoDTO : criteriosCalificacionList) {

			evaluacionCriterioCalificacionRepository.crearEvaluacionCalificacionProceso(id, proceso,
					criterioCalificacionProcesoDTO.getCriterio(), criterioCalificacionProcesoDTO.isAprobo());
			evaluacionCriterioCalificacionRepository.flush();
			id++;
		}

		return tomarDecisionProceso(request);
	}

	public String tomarDecisionProceso(EvaluacionCriterioCalificacionProcesoDTO request) {
		int puntaje = 0;

		for (CriterioCalificacionProcesoDTO criterioCalificacionProcesoDTO : request.getCriterioCalificacionList()) {

			if (criterioCalificacionProcesoDTO.getCriterio().equals(7)
					|| criterioCalificacionProcesoDTO.getCriterio().equals(8)
					|| criterioCalificacionProcesoDTO.getCriterio().equals(9)
					|| criterioCalificacionProcesoDTO.getCriterio().equals(10)) {

				if (!criterioCalificacionProcesoDTO.isAprobo()) {
					procesoRepository.finalizarProceso("REPROBADO", request.getProceso());
					return "El proceso reprobo Por uno de los criterios Excluyentes";
				}

			}

			if (criterioCalificacionProcesoDTO.isAprobo()) {
				puntaje = puntaje + 10;
			}

		}
		if (puntaje >= 80) {
			procesoRepository.finalizarProceso("APROBADO", request.getProceso());
			return "El proceso fue aprobado";
		} else {
			procesoRepository.finalizarProceso("REPROBADO", request.getProceso());
			return "El proceso reprobo por tener un puntaje menor a 80 ";
		}

	}

	// @Transactional(propagation = Propagation.REQUIRES_NEW,isolation =
	// Isolation.READ_COMMITTED)
	public void saveCriterioEvaluacion(Integer id, Integer proceso, Integer criterio, boolean aprobo) {
		Integer ida = evaluacionCriterioCalificacionRepository
				.getLastEvaluacionCriterioCalificacionProcesoRepositoryId();
		evaluacionCriterioCalificacionRepository.crearEvaluacionCalificacionProceso(ida, proceso, criterio, aprobo);
	}

	@Override
	public List<EvaluacionCriterioCalificacionProcesoGridDTO> getEvaluacionCriteriosCalificacionProceso(
			Integer numeroProceso) {

		List<EvaluacionCriterioCalificacionProcesoDTOEntity> evaluacionCriterioCalificacionProcesoDTOEntityList = evaluacionCriterioCalificacionProcesoFacadeRepository
				.getListaEvaluacionCriteriosCalificacion(numeroProceso);

		List evaluacionCriterioCalificacionProcesoDTOList = new ArrayList<EvaluacionCriterioCalificacionProcesoDTO>();
		EvaluacionCriterioCalificacionProcesoGridDTO evaluacionCriterioCalificacionProcesoGridDTO;

		for (EvaluacionCriterioCalificacionProcesoDTOEntity evaluacionCriterioCalificacionProcesoDTOEntity : evaluacionCriterioCalificacionProcesoDTOEntityList) {
			evaluacionCriterioCalificacionProcesoGridDTO = new EvaluacionCriterioCalificacionProcesoGridDTO();
			BeanUtils.copyProperties(evaluacionCriterioCalificacionProcesoDTOEntity,
					evaluacionCriterioCalificacionProcesoGridDTO);
			evaluacionCriterioCalificacionProcesoDTOList.add(evaluacionCriterioCalificacionProcesoGridDTO);
		}

		return evaluacionCriterioCalificacionProcesoDTOList;
	}

}
