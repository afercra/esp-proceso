package gov.mil.esp.proceso.domain.business.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import gov.mil.esp.proceso.domain.dto.GridAnalista;
import gov.mil.esp.proceso.domain.dto.GridAnalistaRequerimientos;
import gov.mil.esp.proceso.domain.dto.GridRequerimientosTotalizador;
import gov.mil.esp.proceso.persistence.GridAnalistaRepository;
import gov.mil.esp.proceso.persistence.GridAnalistaRequerimientoRepository;
import gov.mil.esp.proceso.persistence.GridTotalizadorRepository;
import gov.mil.esp.proceso.persistence.entity.GridAnalistaPersistenceEntity;
import gov.mil.esp.proceso.persistence.entity.GridAnalistaRequerimientosPersistenceEntity;

@Component
public class GridAnalistaBussinessImpl {

	private GridAnalistaRepository gridAnalistaRepository;
	private GridAnalistaRequerimientoRepository gridAnalistaRequerimientoRepository;
	private GridTotalizadorRepository gridTotalizador;
	
	@Autowired
	public GridAnalistaBussinessImpl(GridAnalistaRepository gridAnalistaRepository,
			GridAnalistaRequerimientoRepository gridAnalistaRequerimientoRepository,
			GridTotalizadorRepository gridTotalizador) {
		this.gridAnalistaRepository = gridAnalistaRepository;
		this.gridAnalistaRequerimientoRepository = gridAnalistaRequerimientoRepository;
		this.gridTotalizador = gridTotalizador;
	}

	public List<GridAnalista> listarAspirantes(boolean formularioDiligenciado) {

		List<GridAnalistaPersistenceEntity> gridAnalistalista = gridAnalistaRepository
				.listarAspirantes(formularioDiligenciado);
		List<GridAnalista> listaAnalistas = transformarDTOEnVO(gridAnalistalista);

		return listaAnalistas;

	}

	public List<GridAnalista> listarAspirantesAvalados() {
		return transformarDTOEnVO(gridAnalistaRepository.listarAspirantesAvalados());
	}

	public List<GridAnalistaRequerimientos> listarRequerimientosAnalista() {

		List<GridAnalistaRequerimientosPersistenceEntity> gridAnalistaRequerimientosPersistenceEntityList = gridAnalistaRequerimientoRepository
				.listarRequerimientosAnalista();
		return transformarGridAnalistaRequerimientosEntityEnVO(gridAnalistaRequerimientosPersistenceEntityList);
	}

	public List<GridAnalistaRequerimientos> listarRequerimientosTotalizador() {

		List<GridAnalistaRequerimientosPersistenceEntity> gridAnalistaRequerimientosPersistenceEntityList = gridAnalistaRequerimientoRepository
				.listarRequerimientosTotalizador();
		return transformarGridAnalistaRequerimientosEntityEnVO(gridAnalistaRequerimientosPersistenceEntityList);
	}

	public List<GridRequerimientosTotalizador> listarProcesosDeRequerimiento(String requerimiento) {
		List<GridRequerimientosTotalizador> gridAnalistaList = new ArrayList<GridRequerimientosTotalizador>();
		System.out.println("REQUERIMIENTO ------------------>");
		System.out.println(requerimiento);

		gridTotalizador.listarProcesosPorRequerimiento(requerimiento).forEach(x -> {
			GridRequerimientosTotalizador ga = new GridRequerimientosTotalizador();
			BeanUtils.copyProperties(x, ga);
			gridAnalistaList.add(ga);
		});

		return gridAnalistaList;
	}

	public List<GridAnalistaRequerimientos> transformarGridAnalistaRequerimientosEntityEnVO(
			List<GridAnalistaRequerimientosPersistenceEntity> listaEntity) {
		List<GridAnalistaRequerimientos> lista = new ArrayList<>();
		for (GridAnalistaRequerimientosPersistenceEntity gridAnalistaPersistenceEntity : listaEntity) {

			GridAnalistaRequerimientos gridAnalistaRequerimientos = new GridAnalistaRequerimientos();
			BeanUtils.copyProperties(gridAnalistaPersistenceEntity, gridAnalistaRequerimientos);
			lista.add(gridAnalistaRequerimientos);
		}

		return lista;
	}

	public List<GridAnalista> transformarDTOEnVO(List<GridAnalistaPersistenceEntity> listaDTO) {
		List<GridAnalista> lista = new ArrayList<>();
		for (GridAnalistaPersistenceEntity gridAnalistaRepository : listaDTO) {

			GridAnalista gridAnalista = new GridAnalista();
			BeanUtils.copyProperties(gridAnalistaRepository, gridAnalista);
			lista.add(gridAnalista);
		}

		return lista;
	}

	public List<GridAnalista> getProcesosEntrevistador(String entrevistador) {
		List<GridAnalistaPersistenceEntity> listaProcesosPersistenceEntity = gridAnalistaRepository
				.getProcesosEntrevistador(entrevistador);
		List<GridAnalista> listaProcesos = new ArrayList<>();
		for (GridAnalistaPersistenceEntity gridAnalistaRepository : listaProcesosPersistenceEntity) {

			GridAnalista gridAnalista = new GridAnalista();
			BeanUtils.copyProperties(gridAnalistaRepository, gridAnalista);
			listaProcesos.add(gridAnalista);
		}

		return listaProcesos;

	}

}
