package gov.mil.esp.proceso.domain.business.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import gov.mil.esp.proceso.domain.business.ProcesoBusiness;
import gov.mil.esp.proceso.domain.dto.AsignarProcesoAEntrevistadorDTO;
import gov.mil.esp.proceso.domain.dto.CambioDecisionProcesoDTO;
import gov.mil.esp.proceso.domain.exception.EntrevistadorNoExisteException;
import gov.mil.esp.proceso.domain.exception.ProcesoEnCursoException;
import gov.mil.esp.proceso.domain.exception.ProcesoNoAsignableException;
import gov.mil.esp.proceso.domain.exception.ProcesoNoAsignadoException;
import gov.mil.esp.proceso.middleware.producer.ProcesoProducer;
import gov.mil.esp.proceso.model.Proceso;
import gov.mil.esp.proceso.persistence.DecisionProcesoRepository;
import gov.mil.esp.proceso.persistence.EstadoProcesoRepository;
import gov.mil.esp.proceso.persistence.ParametroProcesoRepository;
import gov.mil.esp.proceso.persistence.PersonaRepository;
import gov.mil.esp.proceso.persistence.ProcesoRepository;
import gov.mil.esp.proceso.persistence.entity.DecisionProcesoPersistenceEntity;
import gov.mil.esp.proceso.persistence.entity.EstadoProcesoPersistenceEntity;
import gov.mil.esp.proceso.persistence.entity.ParametroProcesoPersistenceEntity;
import gov.mil.esp.proceso.persistence.entity.PersonaPersistenceEntity;
import gov.mil.esp.proceso.persistence.entity.ProcesoPersistenceEntity;

@Component
public class ProcesoBusinessImpl implements ProcesoBusiness{

	private ParametroProcesoRepository parametroProcesoRepository;
	private PersonaRepository personaRepository;
	private ProcesoRepository procesoRepository;
	private EstadoProcesoRepository estadoProcesoRepository;

	private DecisionProcesoRepository decisionProcesoRepository;
	private Logger logger = LoggerFactory.getLogger(ProcesoBusinessImpl.class);
	private HashMap<String, String> parametrosProceso;
	private HashMap<String, EstadoProcesoPersistenceEntity> estadosProceso;
	private HashMap<String, DecisionProcesoPersistenceEntity> decisionesProceso;

	@Autowired
	public ProcesoBusinessImpl(ParametroProcesoRepository parametroProcesoRepository, PersonaRepository aspiranteRepository,
			DecisionProcesoRepository decisionProcesoRepository, ProcesoProducer procesoProducer,
			EstadoProcesoRepository estadoProcesoRepository, ProcesoRepository procesoRepository) {
		this.parametroProcesoRepository = parametroProcesoRepository;
		this.personaRepository = aspiranteRepository;
		this.procesoRepository = procesoRepository;
		this.estadoProcesoRepository = estadoProcesoRepository;
		this.decisionProcesoRepository = decisionProcesoRepository;
	}

	@PostConstruct
	private void onInit() {
		parametrosProceso = new HashMap<String, String>();
		estadosProceso = new HashMap<String, EstadoProcesoPersistenceEntity>();
		decisionesProceso = new HashMap<String, DecisionProcesoPersistenceEntity>();
		getEstadosProceso();
		getDecisionesProceso();
	}

	@Transactional
	public Proceso iniciarProceso(String documentoIdentidad) throws NoSuchElementException, ProcesoEnCursoException {
		String nombreProceso = estructurarNumeroProceso(documentoIdentidad);

		if (procesoRepository.verificarProcesoActivoPersona(documentoIdentidad) > 0) {
			throw new ProcesoEnCursoException("Existe un proceso activo para el documento: " + documentoIdentidad);
		}

		Calendar cal = Calendar.getInstance();

		ProcesoPersistenceEntity proceso = new ProcesoPersistenceEntity(cal.getTime(), nombreProceso);
		Optional<PersonaPersistenceEntity> personaOptional = personaRepository.findById(documentoIdentidad);
		PersonaPersistenceEntity personaProceso = personaOptional.get();

		Integer lastProcessId = procesoRepository.getLastProcessId();

		proceso.setNumeroProceso(lastProcessId + 1);
		proceso.setPersona(personaProceso);
		proceso.setEntrevistador(null);
		proceso.setEstadoProceso(estadosProceso.get("ACT"));
		ProcesoPersistenceEntity procesoPersistenceEntityReturn = procesoRepository.save(proceso);

		logger.info("Se creo el proceso " + nombreProceso);
		Proceso procesoRetorno = new Proceso();
		BeanUtils.copyProperties(procesoPersistenceEntityReturn, procesoRetorno);

		return procesoRetorno;
	}

	public void getEstadosProceso() {
		List<gov.mil.esp.proceso.persistence.entity.EstadoProcesoPersistenceEntity> estadosProcesoList;
		estadosProcesoList = estadoProcesoRepository.findAll();
		for (gov.mil.esp.proceso.persistence.entity.EstadoProcesoPersistenceEntity estadoProceso : estadosProcesoList) {
			estadosProceso.put(estadoProceso.getId(), estadoProceso);
		}
	}

	public void getDecisionesProceso() {
		List<DecisionProcesoPersistenceEntity> decisionesProcesoList;
		decisionesProcesoList = decisionProcesoRepository.findAll();
		for (DecisionProcesoPersistenceEntity decisionProceso : decisionesProcesoList) {
			decisionesProceso.put(decisionProceso.getNombre(), decisionProceso);
		}
	}

	public HashMap<String, String> getParametrosProceso() {
		List<ParametroProcesoPersistenceEntity> parametros;
		parametros = parametroProcesoRepository.findAll();
		for (ParametroProcesoPersistenceEntity parametroProceso : parametros) {
			parametrosProceso.put(parametroProceso.getNombre(), parametroProceso.getValor());
		}

		return parametrosProceso;
	}

	public String estructurarNumeroProceso(String numeroDocumento) {
		getParametrosProceso();

		String identificadorProceso = "";
		String prefijoProceso = parametrosProceso.get("prefijo.proceso");
		String anoProceso = parametrosProceso.get("ano");
		identificadorProceso = prefijoProceso + "-" + anoProceso + "-" + procesoRepository.getNextConsecutivoProceso()
				+ "-" + numeroDocumento;
		return identificadorProceso;
	};

	@Transactional
	public List<Proceso> cambiarDecisionProceso(List<CambioDecisionProcesoDTO> procesos) {

		for (CambioDecisionProcesoDTO cambioEstadoProcesoDTO : procesos) {
			if (cambioEstadoProcesoDTO != null) {
				procesoRepository.actualizarDecisionProceso(cambioEstadoProcesoDTO.getIdDecision(),
						cambioEstadoProcesoDTO.getNombreProceso());
			}
		}

		List<ProcesoPersistenceEntity> procesoPersitenceEntityList = procesoRepository.findAll();
		List<Proceso> procesoReturnList = new ArrayList<Proceso>();

		for (ProcesoPersistenceEntity procesoPersistenceEntity : procesoPersitenceEntityList) {
			Proceso proceso = new Proceso();
			BeanUtils.copyProperties(procesoPersistenceEntity, proceso);
			procesoReturnList.add(proceso);
		}

		return procesoReturnList;
	}

	@Transactional
	public void asignarProcesoAEntrevistador(AsignarProcesoAEntrevistadorDTO asignarProcesoAEntrevistadorDTO)
			throws ProcesoNoAsignableException, EntrevistadorNoExisteException {

		String nombreProceso = asignarProcesoAEntrevistadorDTO.getProceso();
		String entrevistador = asignarProcesoAEntrevistadorDTO.getEntrevistador();

		if (procesoRepository.verificarExistenciaEntrevistador(entrevistador) <= 0) {
			throw new EntrevistadorNoExisteException("El entrevistador que se busca asignar al proceso no existe");
		}

		if (procesoRepository.verificarProcesoAsignado(nombreProceso) <= 0) {
			throw new ProcesoNoAsignableException(
					"El proceso: " + nombreProceso + " no debe tener entrevistador asignado, debe estar activo");
		}
		
		procesoRepository.asignarProcesoAEntrevistador(entrevistador, nombreProceso);

	}

	@Transactional
	public void desasignarProcesoAEntrevistador(AsignarProcesoAEntrevistadorDTO desasignarProcesoAEntrevistadorDTO)
			throws EntrevistadorNoExisteException, ProcesoNoAsignadoException {

		String nombreProceso = desasignarProcesoAEntrevistadorDTO.getProceso();
		String entrevistador = desasignarProcesoAEntrevistadorDTO.getEntrevistador();

		if (procesoRepository.verificarProcesoAsignadoAEntrevistador(nombreProceso,entrevistador) < 1) {

			throw new ProcesoNoAsignadoException(
					"El entrevistador: " + entrevistador + " no tiene asignado el proceso:" + nombreProceso);
		}

		if (procesoRepository.verificarExistenciaEntrevistador(entrevistador) <= 0) {
			throw new EntrevistadorNoExisteException("El entrevistador al que busca desasignar el proceso no existe");
		}

		procesoRepository.desasignarProcesoAEntrevistador(entrevistador, nombreProceso);

	}

}
