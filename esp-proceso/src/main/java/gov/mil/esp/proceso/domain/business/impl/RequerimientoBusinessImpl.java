package gov.mil.esp.proceso.domain.business.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.mil.esp.proceso.domain.business.RequerimientoBusiness;
import gov.mil.esp.proceso.domain.dto.CambiarEstadoRequerimientoDTO;
import gov.mil.esp.proceso.domain.dto.CambioEstadoRequerimientoDTO;
import gov.mil.esp.proceso.domain.dto.CreacionRequerimientoDTO;
import gov.mil.esp.proceso.model.Requerimiento;
import gov.mil.esp.proceso.persistence.RequerimientoRepository;
import gov.mil.esp.proceso.persistence.entity.RequerimientoPersistenceEntity;

@Service
public class RequerimientoBusinessImpl implements RequerimientoBusiness {

	private RequerimientoRepository requerimientoRepository;

	@Autowired
	public RequerimientoBusinessImpl(RequerimientoRepository requerimientoRepository) {
		this.requerimientoRepository = requerimientoRepository;
	}

	@Transactional
	public Requerimiento crearRequerimiento(List<CreacionRequerimientoDTO> procesoRequest) {

		Integer sequenceId = requerimientoRepository.getNextRequerimientoId();

		Calendar cal = Calendar.getInstance();
		String nombreRequerimiento = estructurarNombreRequerimiento();

		requerimientoRepository.createRequerimiento(cal.getTime(), nombreRequerimiento, sequenceId);

		List<String> nombreProcesosList = new ArrayList<String>();

		for (CreacionRequerimientoDTO creacionRequerimientoDTO : procesoRequest) {
			if (creacionRequerimientoDTO != null) {
				nombreProcesosList.add(creacionRequerimientoDTO.getNombreProceso());
			}
		}

		requerimientoRepository.asociarProcesosARequerimiento(sequenceId, nombreProcesosList);

		Requerimiento requerimiento = new Requerimiento(null, null);
		BeanUtils.copyProperties(
				requerimientoRepository.findById(sequenceId).orElse(new RequerimientoPersistenceEntity()),
				requerimiento);

		return requerimiento;

	}

	@Transactional
	public void eliminarRequerimiento(String nombreRequerimiento) {
		Integer idRequerimiento = requerimientoRepository.getIdFromRequerimientoName(nombreRequerimiento);
		requerimientoRepository.desasociarProcesosDeRequerimiento(idRequerimiento);
		requerimientoRepository.eliminarRequerimiento(nombreRequerimiento);

	}

	@Transactional
	public List<Requerimiento> cambiarEstadoRequerimiento(CambiarEstadoRequerimientoDTO requerimientosRequest) {

		List<CambioEstadoRequerimientoDTO> cambioEstadoReqList = requerimientosRequest.getRequerimientos();

		List<String> listaRequerimientos = new ArrayList<String>();

		for (CambioEstadoRequerimientoDTO cambioEstadoRequerimientoDTO : cambioEstadoReqList) {
			if (cambioEstadoRequerimientoDTO != null) {
				listaRequerimientos.add(cambioEstadoRequerimientoDTO.getNumeroRequerimiento());
			}
		}

		requerimientoRepository.cambiarEstadoRequerimiento(requerimientosRequest.getIdEstado(), listaRequerimientos);

		List<RequerimientoPersistenceEntity> listRequerimientosPersistenceEntity = requerimientoRepository.findAll();
		List<Requerimiento> listRequerimientos = new ArrayList<Requerimiento>();

		for (RequerimientoPersistenceEntity requerimientoPersistenceEntity : listRequerimientosPersistenceEntity) {
			Requerimiento req = new Requerimiento();
			BeanUtils.copyProperties(requerimientoPersistenceEntity, req);
			listRequerimientos.add(req);
		}
		return listRequerimientos;
	}

	public String estructurarNombreRequerimiento() {

		String identificadorProceso = "";
		long currentMilis = System.currentTimeMillis();
		String currentMilisString = Long.toString(currentMilis);

		identificadorProceso = "REQ" + "-" + currentMilisString;
		return identificadorProceso;
	};

}
