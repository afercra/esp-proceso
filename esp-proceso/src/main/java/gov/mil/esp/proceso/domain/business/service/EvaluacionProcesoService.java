package gov.mil.esp.proceso.domain.business.service;

import gov.mil.esp.proceso.domain.Response;
import gov.mil.esp.proceso.domain.dto.EvaluacionCriterioCalificacionProcesoDTO;

public interface EvaluacionProcesoService {

	public Response<?> saveEvaluacionCriterios(EvaluacionCriterioCalificacionProcesoDTO request);

	public Response<?> getEvaluacionCriteriosCalificacionProceso(Integer numeroProceso);

}
