package gov.mil.esp.proceso.domain.business.service;

import java.util.List;

import gov.mil.esp.proceso.domain.Response;
import gov.mil.esp.proceso.domain.dto.GridAnalista;
import gov.mil.esp.proceso.domain.dto.GridAnalistaRequerimientos;
import gov.mil.esp.proceso.domain.dto.GridRequerimientosTotalizador;

public interface GridAnalistaService {

	public Response<List<GridAnalista>> listarProcesos(boolean formularioDiligenciado);

	public Response<List<GridAnalista>> listarAvalados();

	public Response<List<GridAnalistaRequerimientos>> listarRequerimientos();

	public Response<List<GridAnalistaRequerimientos>> listarRequerimientosTotalizador();

	public Response<List<GridRequerimientosTotalizador>> listarProcesosDeRequerimiento(String requerimiento);

	public Response<List<GridAnalista>> getProcesosEntrevistador(String entrevistador);

}
