package gov.mil.esp.proceso.domain.business.service;

import java.util.List;
import java.util.NoSuchElementException;

import gov.mil.esp.proceso.domain.Response;
import gov.mil.esp.proceso.domain.dto.AsignarProcesoAEntrevistadorDTO;
import gov.mil.esp.proceso.domain.dto.CambiarDecisionProcesoDTO;
import gov.mil.esp.proceso.domain.dto.Request;
import gov.mil.esp.proceso.domain.exception.EntrevistadorNoExisteException;
import gov.mil.esp.proceso.domain.exception.ProcesoEnCursoException;
import gov.mil.esp.proceso.domain.exception.ProcesoNoAsignableException;
import gov.mil.esp.proceso.domain.exception.ProcesoNoAsignadoException;
import gov.mil.esp.proceso.model.Proceso;

public interface ProcesoService {

	public Response<Proceso> enviarIdentificacionAColaInicioProceso(String numeroDocumento);

	public Response<Proceso> crearProceso(String documentoIdentidad)
			throws NoSuchElementException, ProcesoEnCursoException;

	public Response<List<Proceso>> cambiarDecisionProceso(Request<CambiarDecisionProcesoDTO> cambiarEstadoProcesoRequest);

	public Response<?> asignarProcesoAEntrevistador(Request<AsignarProcesoAEntrevistadorDTO> request)
			throws ProcesoNoAsignableException, EntrevistadorNoExisteException;

	public Response<?> desAsignarProcesoAEntrevistador(Request<AsignarProcesoAEntrevistadorDTO> request)
			throws ProcesoNoAsignableException, EntrevistadorNoExisteException, ProcesoNoAsignadoException;


}
