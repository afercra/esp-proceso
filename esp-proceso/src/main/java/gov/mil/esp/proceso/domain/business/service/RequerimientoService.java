package gov.mil.esp.proceso.domain.business.service;

import java.util.List;

import gov.mil.esp.proceso.domain.Response;
import gov.mil.esp.proceso.domain.dto.CambiarEstadoRequerimientoDTO;
import gov.mil.esp.proceso.domain.dto.CreacionRequerimientoDTO;
import gov.mil.esp.proceso.domain.dto.Request;
import gov.mil.esp.proceso.model.Requerimiento;

public interface RequerimientoService {

	public Response<Requerimiento> crearRequerimiento(Request<List<CreacionRequerimientoDTO>> procesosList);

	public Response<List<Requerimiento>> cambiarEstadoRequerimiento(
			Request<CambiarEstadoRequerimientoDTO> requerimientosRequest);

	public Response<Requerimiento> eliminarRequerimiento(String nombreRequerimiento);

}
