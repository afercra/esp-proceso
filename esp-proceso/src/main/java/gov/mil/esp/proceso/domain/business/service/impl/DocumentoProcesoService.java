package gov.mil.esp.proceso.domain.business.service.impl;

import org.postgresql.util.PSQLException;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import gov.mil.esp.proceso.domain.Response;
import gov.mil.esp.proceso.domain.business.impl.DocumentoProcesoBusinessImpl;
import gov.mil.esp.proceso.domain.dto.CrearDocumentoDTO;
import gov.mil.esp.proceso.domain.dto.DocumentoProcesoDTO;
import gov.mil.esp.proceso.domain.dto.Request;
import gov.mil.esp.proceso.domain.exception.DocumentoNoexisteException;
import gov.mil.esp.proceso.domain.exception.ProcesoNoExisteException;

@Service
public class DocumentoProcesoService {
	
	private DocumentoProcesoBusinessImpl documentoProcesoLogic;

	
	public DocumentoProcesoService(DocumentoProcesoBusinessImpl documentoProcesoLogic) {
		this.documentoProcesoLogic = documentoProcesoLogic;
	}
	
	public Response<?> uploadDocument(Request<CrearDocumentoDTO> crearDocumentoRequest) throws PSQLException, ProcesoNoExisteException { 
		documentoProcesoLogic.uploadDocument(crearDocumentoRequest.getMensaje());
		return null;
	}
	
	
    public Response<?> getDocumento(String nombreProceso, String nombreDocumento) throws DocumentoNoexisteException { 
    	Response<DocumentoProcesoDTO> response = new Response<DocumentoProcesoDTO>();
    	DocumentoProcesoDTO documentoProcesoDTO = new DocumentoProcesoDTO();
    	BeanUtils.copyProperties(documentoProcesoLogic.getDocumento(nombreProceso, nombreDocumento), documentoProcesoDTO);
    	
    	response.setMensaje("El documento es :");
    	response.setPayload(documentoProcesoDTO); 	
		return response;
	}
	
}
