package gov.mil.esp.proceso.domain.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gov.mil.esp.proceso.domain.Response;
import gov.mil.esp.proceso.domain.business.EvaluacionProcesoBusiness;
import gov.mil.esp.proceso.domain.business.impl.EvaluacionProcesoBusinessImpl;
import gov.mil.esp.proceso.domain.business.service.EvaluacionProcesoService;
import gov.mil.esp.proceso.domain.dto.EvaluacionCriterioCalificacionProcesoDTO;
import gov.mil.esp.proceso.domain.dto.EvaluacionCriterioCalificacionProcesoGridDTO;

@Service
public class EvaluacionProcesoServiceImpl implements EvaluacionProcesoService {

	private EvaluacionProcesoBusiness evaluacionProcesoBusiness;
	
	@Autowired
    public EvaluacionProcesoServiceImpl(EvaluacionProcesoBusinessImpl evaluacionProcesoBusinessImpl) {
    	this.evaluacionProcesoBusiness = evaluacionProcesoBusinessImpl;
    }
	
	@Override
	public Response<?> saveEvaluacionCriterios(EvaluacionCriterioCalificacionProcesoDTO request) {
		
		Response<String> response = new Response<String>();
		response.setMensaje(evaluacionProcesoBusiness.saveEvaluacionCriterios(request));
		return response;
	}

	@Override
	public Response<?> getEvaluacionCriteriosCalificacionProceso(Integer numeroProceso) {
		Response<List<EvaluacionCriterioCalificacionProcesoGridDTO>> response =  new Response<List<EvaluacionCriterioCalificacionProcesoGridDTO>>();
		response.setPayload(evaluacionProcesoBusiness.getEvaluacionCriteriosCalificacionProceso(numeroProceso));
		response.setMensaje("Los criterios para el proceso son:");
		return response;
	}
	
	

}
