package gov.mil.esp.proceso.domain.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import gov.mil.esp.proceso.domain.Response;
import gov.mil.esp.proceso.domain.business.impl.GridAnalistaBussinessImpl;
import gov.mil.esp.proceso.domain.business.service.GridAnalistaService;
import gov.mil.esp.proceso.domain.dto.GridAnalista;
import gov.mil.esp.proceso.domain.dto.GridAnalistaRequerimientos;
import gov.mil.esp.proceso.domain.dto.GridRequerimientosTotalizador;

@Service
public class GridAnalistaServiceImpl implements GridAnalistaService {

	private Response<List<GridAnalista>> response;

	private Response<List<GridAnalistaRequerimientos>> responseReq;
	
	private Response<List<GridRequerimientosTotalizador>> responseTotalizador;

	private GridAnalistaBussinessImpl gridAnalistaLogic;

	@Autowired
	public GridAnalistaServiceImpl(GridAnalistaBussinessImpl gridAnalistaLogic) {
		this.gridAnalistaLogic = gridAnalistaLogic;
	}

	public Response<List<GridAnalista>> listarProcesos(boolean formularioDiligenciado) {
		List<GridAnalista> gridAnalista = gridAnalistaLogic.listarAspirantes(formularioDiligenciado);
		buildResponseMessage(HttpStatus.OK, "200", "La lista es: ", gridAnalista);
		return response;
	}

	public Response<List<GridAnalista>> listarAvalados() {
		List<GridAnalista> gridAnalista = gridAnalistaLogic.listarAspirantesAvalados();
		buildResponseMessage(HttpStatus.OK, "200", "La lista es: ", gridAnalista);
		return response;
	}

	public Response<List<GridAnalistaRequerimientos>> listarRequerimientos() {
		List<GridAnalistaRequerimientos> gridAnalista = gridAnalistaLogic.listarRequerimientosAnalista();
		buildResponseMessageReq(HttpStatus.OK, "200", "La lista es: ", gridAnalista);
		return responseReq;
	}

	public Response<List<GridAnalistaRequerimientos>> listarRequerimientosTotalizador() {
		List<GridAnalistaRequerimientos> gridTotalizador = gridAnalistaLogic.listarRequerimientosTotalizador();
		buildResponseMessageReq(HttpStatus.OK, "200", "La lista es: ", gridTotalizador);
		return responseReq;
	}

	public Response<List<GridRequerimientosTotalizador>> listarProcesosDeRequerimiento(String requerimiento) {
		List<GridRequerimientosTotalizador> gridAnalista = gridAnalistaLogic.listarProcesosDeRequerimiento(requerimiento);
		buildResponseMessageTotalizador(HttpStatus.OK, "200", "La lista es: ", gridAnalista);
		return responseTotalizador;
	}

	/*
	 * public Response<List<GridAnalista>> listarProcesoPordecisionProceso(boolean
	 * formularioDiligenciado) { List<GridAnalista> gridAnalista =
	 * gridAnalistaLogic.listarAspirantes(formularioDiligenciado);
	 * buildResponseMessage(HttpStatus.OK ,"200", "La lista es: ", gridAnalista);
	 * return response; }
	 */

	public Response<List<GridAnalista>> getProcesosEntrevistador(String entrevistador) {
		List<GridAnalista> gridAnalista = gridAnalistaLogic.getProcesosEntrevistador(entrevistador);
		buildResponseMessage(HttpStatus.OK, "200", "La lista es: ", gridAnalista);
		return response;
	}

	private void buildResponseMessageReq(HttpStatus httpStatus, String codigo, String mensaje,
			List<GridAnalistaRequerimientos> payload) {
		responseReq = new Response<List<GridAnalistaRequerimientos>>();
		responseReq.setCodigo(codigo);
		responseReq.setMensaje(mensaje);
		responseReq.setPayload(payload);

	}

	private void buildResponseMessage(HttpStatus httpStatus, String codigo, String mensaje,
			List<GridAnalista> payload) {
		response = new Response<List<GridAnalista>>();
		response.setCodigo(codigo);
		response.setMensaje(mensaje);
		response.setPayload(payload);

	}
	
	private void buildResponseMessageTotalizador(HttpStatus httpStatus, String codigo, String mensaje,
			List<GridRequerimientosTotalizador> payload) {
		responseTotalizador = new Response<List<GridRequerimientosTotalizador>>();
		responseTotalizador.setCodigo(codigo);
		responseTotalizador.setMensaje(mensaje);
		responseTotalizador.setPayload(payload);

	}

}
