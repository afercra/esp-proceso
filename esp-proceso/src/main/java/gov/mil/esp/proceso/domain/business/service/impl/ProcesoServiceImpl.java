package gov.mil.esp.proceso.domain.business.service.impl;

import java.util.List;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gov.mil.esp.proceso.domain.Response;
import gov.mil.esp.proceso.domain.business.ProcesoBusiness;
import gov.mil.esp.proceso.domain.business.impl.ProcesoBusinessImpl;
import gov.mil.esp.proceso.domain.business.service.ProcesoService;
import gov.mil.esp.proceso.domain.dto.AsignarProcesoAEntrevistadorDTO;
import gov.mil.esp.proceso.domain.dto.CambiarDecisionProcesoDTO;
import gov.mil.esp.proceso.domain.dto.Request;
import gov.mil.esp.proceso.domain.exception.EntrevistadorNoExisteException;
import gov.mil.esp.proceso.domain.exception.ProcesoEnCursoException;
import gov.mil.esp.proceso.domain.exception.ProcesoNoAsignableException;
import gov.mil.esp.proceso.domain.exception.ProcesoNoAsignadoException;
import gov.mil.esp.proceso.model.Proceso;

@Service
public class ProcesoServiceImpl implements ProcesoService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProcesoServiceImpl.class);

	private Response<Proceso> response;
	private Response<List<Proceso>> responseList;
	private ProcesoBusiness procesoBusiness;

	@Autowired
	public ProcesoServiceImpl(ProcesoBusinessImpl procesoLogic) {
		this.procesoBusiness = procesoLogic;
	}

	// procesoLogic.enviarIdentificacionAColaInicioProceso(numeroDocumento);
	public Response<Proceso> enviarIdentificacionAColaInicioProceso(String numeroDocumento) {
		buildResponseMessage("200", "Se ha generado una solicitud de proceso para el número de documento: "
				+ numeroDocumento + ", por favor revise su bandeja para ver el estado.", null);
		return response;
	}

	public Response<Proceso> crearProceso(String documentoIdentidad)
			throws NoSuchElementException, ProcesoEnCursoException {
		Proceso proceso = procesoBusiness.iniciarProceso(documentoIdentidad);
		buildResponseMessage("200", "Se genero el proceso: " + proceso.getNombreProceso(), proceso);
		return response;
	}

	public Response<List<Proceso>> cambiarDecisionProceso(
			Request<CambiarDecisionProcesoDTO> cambiarEstadoProcesoRequest) {
		CambiarDecisionProcesoDTO cambiarEstadoProcesoDTO = cambiarEstadoProcesoRequest.getMensaje();
		buildResponseMessageList("200", "Se actualizaron los procesos: ",
				procesoBusiness.cambiarDecisionProceso(cambiarEstadoProcesoDTO.getProcesos()));
		return responseList;
	}

	public Response<?> asignarProcesoAEntrevistador(Request<AsignarProcesoAEntrevistadorDTO> request)
			throws ProcesoNoAsignableException, EntrevistadorNoExisteException {

		procesoBusiness.asignarProcesoAEntrevistador(request.getMensaje());
		Response<String> response = new Response<String>();
		response.setMensaje("Se asigno el proceso: " + request.getMensaje().getProceso() + "al entrevistador :"
				+ request.getMensaje().getEntrevistador());
		return response;
	}

	public Response<?> desAsignarProcesoAEntrevistador(Request<AsignarProcesoAEntrevistadorDTO> request)
			throws ProcesoNoAsignableException, EntrevistadorNoExisteException, ProcesoNoAsignadoException {
		procesoBusiness.desasignarProcesoAEntrevistador(request.getMensaje());
		Response<String> response = new Response<String>();
		response.setMensaje("Se desasigno el proceso: " + request.getMensaje().getProceso() + "al entrevistador :"
				+ request.getMensaje().getEntrevistador());
		return response;
	}

	public Response<?> getProcesosEntrevistador() {

		return response;
	}

	private void buildResponseMessageList(String codigo, String mensaje, List<Proceso> payload) {
		responseList = new Response<List<Proceso>>();
		responseList.setCodigo(codigo);
		responseList.setMensaje(mensaje);
		responseList.setPayload(payload);

	}

	private void buildResponseMessage(String codigo, String mensaje, Proceso payload) {
		response = new Response<Proceso>();
		response.setCodigo(codigo);
		response.setMensaje(mensaje);
		response.setPayload(payload);

	}

}
