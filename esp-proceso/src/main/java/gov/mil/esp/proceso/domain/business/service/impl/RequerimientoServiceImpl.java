package gov.mil.esp.proceso.domain.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import gov.mil.esp.proceso.domain.Response;
import gov.mil.esp.proceso.domain.business.impl.RequerimientoBusinessImpl;
import gov.mil.esp.proceso.domain.business.service.RequerimientoService;
import gov.mil.esp.proceso.domain.dto.CambiarEstadoRequerimientoDTO;
import gov.mil.esp.proceso.domain.dto.CreacionRequerimientoDTO;
import gov.mil.esp.proceso.domain.dto.Request;
import gov.mil.esp.proceso.model.Requerimiento;

@Component
public class RequerimientoServiceImpl implements RequerimientoService {

	private Response<Requerimiento> response;
	private Response<List<Requerimiento>> responseList;
	private RequerimientoBusinessImpl requerimientoBusinessImpl;

	@Autowired
	public RequerimientoServiceImpl(RequerimientoBusinessImpl requerimientoLogic) {
		this.requerimientoBusinessImpl = requerimientoLogic;
	}

	public Response<Requerimiento> crearRequerimiento(Request<List<CreacionRequerimientoDTO>> procesosList) {
		buildResponseMessage("200", "Se  creo el requerimiento: ",
				requerimientoBusinessImpl.crearRequerimiento(procesosList.getMensaje()));
		return response;
	}

	public Response<List<Requerimiento>> cambiarEstadoRequerimiento(
			Request<CambiarEstadoRequerimientoDTO> requerimientosRequest) {
		buildResponseMessageList("200", "Se  creo el requerimiento: ",
				requerimientoBusinessImpl.cambiarEstadoRequerimiento(requerimientosRequest.getMensaje()));
		return responseList;
	}

	public Response<Requerimiento> eliminarRequerimiento(String nombreRequerimiento) {
		requerimientoBusinessImpl.eliminarRequerimiento(nombreRequerimiento);
		buildResponseMessage("200", "Se  elimino el requerimiento: " + nombreRequerimiento, null);
		return response;
	}

	private void buildResponseMessageList(String codigo, String mensaje, List<Requerimiento> payload) {
		responseList = new Response<List<Requerimiento>>();
		responseList.setCodigo(codigo);
		responseList.setMensaje(mensaje);
		responseList.setPayload(payload);
	}

	private void buildResponseMessage(String codigo, String mensaje, Requerimiento payload) {
		response = new Response<Requerimiento>();
		response.setCodigo(codigo);
		response.setMensaje(mensaje);
		response.setPayload(payload);

	}

}
