package gov.mil.esp.proceso.domain.dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class AsignarProcesoAEntrevistadorDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@NotBlank(message = "debe enviar usuario entrevistador para asignar proceso no vacio")
	@NotNull(message = "debe enviar usuario entrevistador para asignar proceso")
	private String entrevistador;
	
	@NotBlank(message = "debe enviar un nombre de proceso no vacio")
	@NotNull(message = "debe enviar un nombre de proceso no vacio")
	private String proceso;

}
