package gov.mil.esp.proceso.domain.dto;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CambiarDecisionProcesoDTO implements Serializable{

	private static final long serialVersionUID = 1L;

	@NotBlank(message = "Debe Enviar un numero de proceso ")
	@NotNull(message= "Debe enviar un numero de Proceso")
	private List<CambioDecisionProcesoDTO> procesos;
	
}
