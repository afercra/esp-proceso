package gov.mil.esp.proceso.domain.dto;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class CambiarEstadoRequerimientoDTO implements Serializable {

	
	private static final long serialVersionUID = 1L;

	
	@NotNull(message= "Debe enviar un numero de Proceso")
	private List<CambioEstadoRequerimientoDTO> requerimientos;
	

	@NotNull(message= "Debe Enviar una decision de Proceso")
	private Integer idEstado;
}
