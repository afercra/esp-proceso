package gov.mil.esp.proceso.domain.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CambioDecisionProcesoDTO {
	
	@NotBlank(message = "El campo nombreProceso no puede estar en blanco")
	@NotNull(message = "El campo nombreProceso no puede estar vacio")
	String nombreProceso;
	
	@NotBlank(message = "Debe Enviar una decision de Proceso")
	@NotNull(message= "Debe Enviar una decision de Proceso")
	private String idDecision;

}
