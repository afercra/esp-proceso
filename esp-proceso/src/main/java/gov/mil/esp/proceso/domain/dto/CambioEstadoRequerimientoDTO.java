package gov.mil.esp.proceso.domain.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CambioEstadoRequerimientoDTO {

	@NotBlank(message = "El campo numeroRequerimiento no puede estar en blanco")
	@NotNull(message = "El campo numeroRequerimiento no puede estar vacio")
	private String numeroRequerimiento;
}
