package gov.mil.esp.proceso.domain.dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreacionProcesoDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@NotBlank(message = "El campo documento de Identidad no puede estar en blanco")
	@NotNull(message = "El campo documento de Identidad no puede estar vacio")
	private String documentoIdentidad;
	
	
	

}
