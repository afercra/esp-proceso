package gov.mil.esp.proceso.domain.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class CrearDocumentoDTO {

	@NotNull(message = "Debe enviar el valor para nombreDocumento")
	@NotBlank(message = "Debe enviar el valor para nombreDocumento")
	private String nombreDocumento;
	
	@NotNull(message = "Debe enviar el valor para numeroProceso")
	Integer numeroProceso;

	@NotNull(message = "Debe ajuntar un documento")
	byte[] documento;

	String observaciones;

	boolean tieneAntecedentes;

}
