package gov.mil.esp.proceso.domain.dto;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class CriterioCalificacionProcesoDTO {
	
	@NotNull(message = "Debe ingresar el criterio que se va evaluar")
	private Integer criterio;
	
	@NotNull(message = "Debe ingresar si aprobo o no aprobo la persona")
	private boolean aprobo;

	
}
