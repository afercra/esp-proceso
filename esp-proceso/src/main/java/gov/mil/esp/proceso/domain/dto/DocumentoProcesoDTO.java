package gov.mil.esp.proceso.domain.dto;

import lombok.Data;

@Data
public class DocumentoProcesoDTO {
	
	
	private byte [] documento;
	private boolean tieneAntecedentes;
	private String observaciones;
	
}
