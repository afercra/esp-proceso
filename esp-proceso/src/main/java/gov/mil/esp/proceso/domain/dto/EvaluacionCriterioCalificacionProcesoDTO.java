package gov.mil.esp.proceso.domain.dto;

import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class EvaluacionCriterioCalificacionProcesoDTO {

	@NotNull(message="Debe ingresar el proceso del cual se deben evaluar los criterios")
	private Integer proceso;
	
	@NotNull(message="Debe la lista de Criterios a calificar")
	private List<CriterioCalificacionProcesoDTO> criterioCalificacionList;
	
}
