package gov.mil.esp.proceso.domain.dto;

import lombok.Data;

@Data
public class EvaluacionCriterioCalificacionProcesoGridDTO {

	private Integer criterio;
	
	private boolean aprobo;

}
