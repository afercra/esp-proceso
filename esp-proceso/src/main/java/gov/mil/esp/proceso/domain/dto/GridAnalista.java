package gov.mil.esp.proceso.domain.dto;

import lombok.Data;

@Data
public class GridAnalista {

	private Integer numeroProceso;
	
	private String nombreProceso;
	
	private String apellidosYNombres;
	
	private String idDecision;
	
	private String decision;

	private String tipoDocumento;
	
	private String nombre;
	
	private String fechaInscripcion;
	
	private String numeroDocumento;
}
