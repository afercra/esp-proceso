package gov.mil.esp.proceso.domain.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class GridAnalistaRequerimientos implements Serializable{
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String numeroRequerimiento;
	private String estadoRequerimiento;
}
