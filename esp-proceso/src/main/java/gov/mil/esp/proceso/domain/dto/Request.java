package gov.mil.esp.proceso.domain.dto;

import java.io.Serializable;

import javax.validation.Valid;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonDeserialize
public class Request<T> implements Serializable {

	private static final long serialVersionUID = 1L;
	@Valid
	private T mensaje;

}
