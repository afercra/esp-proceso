package gov.mil.esp.proceso.domain.exception;

public class DocumentoNoexisteException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DocumentoNoexisteException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DocumentoNoexisteException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public DocumentoNoexisteException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DocumentoNoexisteException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DocumentoNoexisteException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
	
	
}
