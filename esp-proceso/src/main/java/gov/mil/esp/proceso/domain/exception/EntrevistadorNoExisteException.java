package gov.mil.esp.proceso.domain.exception;

public class EntrevistadorNoExisteException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EntrevistadorNoExisteException(String message) {
		super(message);
	}

}
