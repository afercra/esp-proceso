package gov.mil.esp.proceso.domain.exception;

public class ProcesoEnCursoException extends Exception {
	private static final long serialVersionUID = 1L;

	public ProcesoEnCursoException(String message) {
		super(message);
	}
}
