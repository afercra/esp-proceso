package gov.mil.esp.proceso.domain.exception;

public class ProcesoExistenteException extends Exception {

	private static final long serialVersionUID = 1L;

	public ProcesoExistenteException(String message) {
		super(message);
	}

}
