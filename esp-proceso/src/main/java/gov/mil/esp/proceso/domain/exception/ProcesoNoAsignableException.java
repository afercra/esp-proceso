package gov.mil.esp.proceso.domain.exception;

public class ProcesoNoAsignableException extends Exception {

	private static final long serialVersionUID = 1L;

	public ProcesoNoAsignableException(String message) {
		super(message);
	}
}
