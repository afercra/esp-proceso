package gov.mil.esp.proceso.domain.exception;

public class ProcesoNoAsignadoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ProcesoNoAsignadoException(String message) {
		super(message);
	}

}
