package gov.mil.esp.proceso.domain.exception;

public class ProcesoNoExisteException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ProcesoNoExisteException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProcesoNoExisteException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

	public ProcesoNoExisteException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public ProcesoNoExisteException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public ProcesoNoExisteException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}
	
	

	
}
