package gov.mil.esp.proceso.middleware.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import gov.mil.esp.proceso.middleware.procesor.ProcesoProcessor;

@Component
public class ProcesoListener {
	
	private ProcesoProcessor initProcesoProcesor;
	
	@Autowired
	public ProcesoListener(ProcesoProcessor initProcesoProcesor){
		this.initProcesoProcesor = initProcesoProcesor;
	}
	
	@JmsListener(destination="initProceso",containerFactory = "customFactory")
	public void readProcessToInit(String numeroDocumento){
		System.out.println("Proceso en cola ---------------------------->");
		System.out.println(numeroDocumento);
		initProcesoProcesor.iniciarProceso(numeroDocumento);
	}
}
