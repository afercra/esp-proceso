package gov.mil.esp.proceso.middleware.procesor;

import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import gov.mil.esp.proceso.domain.business.impl.ProcesoBusinessImpl;
import gov.mil.esp.proceso.domain.exception.ProcesoEnCursoException;

@Component
public class ProcesoProcessor {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ProcesoProcessor.class);
	
	private ProcesoBusinessImpl procesoLogic;
	
	@Autowired
	public ProcesoProcessor(ProcesoBusinessImpl procesoLogic) {
		this.procesoLogic = procesoLogic;
	}
	
	public void iniciarProceso(String numeroDocumento) {
		try {
			procesoLogic.iniciarProceso(numeroDocumento);
		} catch (NoSuchElementException | ProcesoEnCursoException e) {
			LOGGER.error(e.getMessage(),e);		
		}
	}

}
