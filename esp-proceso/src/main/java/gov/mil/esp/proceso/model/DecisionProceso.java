package gov.mil.esp.proceso.model;

import lombok.Data;

@Data
public class DecisionProceso {

	private int id;
	private String nombre;
	private String descripcion;
	private Proceso proceso;

}
