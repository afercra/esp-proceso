package gov.mil.esp.proceso.model;

import lombok.Data;

@Data
public class DocumentoProceso {

	private Integer idDocumento;
	private String nombreDocumento;
	Integer numeroProceso;
	byte[] documento;
	String observaciones;
	boolean tieneAntecedentes;
}
