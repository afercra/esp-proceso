package gov.mil.esp.proceso.model;


import lombok.Data;

@Data
public class EstadoProceso {

	int idEstadoProceso;

	String nombre;

	String descripcion;

	private Proceso proceso;
	
	private String codigo;

}
