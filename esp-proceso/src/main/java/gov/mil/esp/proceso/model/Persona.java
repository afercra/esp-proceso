package gov.mil.esp.proceso.model;


import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class Persona {

	private String primerApellido;
	private String segundoApellido;
	private String primerNombre;
	private String segundoNombre;
	private String tipoDocumento;

	private String numeroDocumento;

	private Date fechaExpedicionDocumento;
	private String paisExpedicionDocumento;
	private String departamentoExpedicionDocumento;
	private String ciudadExpedicionDocumento;
	private String numeroPasaporte;
	private String departamentoExpedicionPasaporte;
	private String ciudadExpedicionPasaporte;
	private String paisNacimiento;
	private String departamentoNacimiento;
	private String ciudadNacimiento;
	private Date fechaNacimiento;
	private String grupoSanguineo;
	private String ocupacion;
	private String numeroTarjetaProfesional;
	private int estatura;
	private int peso;
	private String direccionActual;
	private String barrioActual;
	private String departamentoDireccionActual;
	private String ciudadDireccionActual;
	private String direccionAnterior;
	private String barrioAnterior;
	private String departamentoDireccionAnterior;
	private String ciudadDireccionAnterior;
	private int numeroCelular;
	private int numeroTelefonoActual;
	private int numeroTelefonoAnterior;
	private String correoElectronico;
	private String redesSociales;

	private List<Proceso> proceso;
	private Usuario usuario;

}
