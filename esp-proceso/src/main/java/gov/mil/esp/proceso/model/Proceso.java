package gov.mil.esp.proceso.model;


import java.util.Date;

import lombok.Data;

@Data
public class Proceso {

	private int numeroProceso;
	private Date fechaInicio;
	private Date fechaFin;
	private DecisionProceso decisionProceso;
	private EstadoProceso estadoProceso;
	Persona persona;
	
	private Usuario entrevistador;
	
	private String nombreProceso;
	
	public Proceso() {}

	
	public Proceso(Date fechaInicio, String nombreProceso) {
		super();
		this.fechaInicio = fechaInicio;
		this.nombreProceso = nombreProceso;
	}

	

}
