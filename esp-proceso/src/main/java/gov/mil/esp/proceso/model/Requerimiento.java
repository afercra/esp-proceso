package gov.mil.esp.proceso.model;

import java.util.Date;

import lombok.Data;

@Data
public class Requerimiento {
	
	
	int id;
	Date fechaEmision;
	String nombreRequerimiento;
	
	public Requerimiento() {
		super();
	}
	

	public Requerimiento( Date fechaEmision, String nombreRequerimiento) {
		super();
		this.fechaEmision = fechaEmision;
		this.nombreRequerimiento = nombreRequerimiento;
	}
	
	
	
}
