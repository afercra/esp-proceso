package gov.mil.esp.proceso.model;

import lombok.Data;

@Data
public class TipoUsuario {

	private int id;
	
	private String nombre;
	
	private String descripción;
	
	Usuario usuario;
}
