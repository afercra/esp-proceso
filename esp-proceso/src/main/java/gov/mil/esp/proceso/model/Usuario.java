package gov.mil.esp.proceso.model;
import lombok.Data;

@Data
public class Usuario{
	
	private String id;	
	private String password;	
	private TipoUsuario tipoUsuario;	
	boolean activo;

	public Usuario(String id, String password, boolean activo) {
		super();
		this.id = id;
		this.password = password;
		this.activo = activo;
	}


}
