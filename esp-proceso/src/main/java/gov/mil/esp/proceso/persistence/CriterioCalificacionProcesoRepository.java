package gov.mil.esp.proceso.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import gov.mil.esp.proceso.persistence.entity.CriterioCalificacionProcesoPersistenceEntity;

public interface CriterioCalificacionProcesoRepository extends JpaRepository<CriterioCalificacionProcesoPersistenceEntity, Integer>{

}
