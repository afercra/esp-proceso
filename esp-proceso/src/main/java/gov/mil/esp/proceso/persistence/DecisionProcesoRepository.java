package gov.mil.esp.proceso.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import gov.mil.esp.proceso.persistence.entity.DecisionProcesoPersistenceEntity;

@Repository
public interface DecisionProcesoRepository extends JpaRepository<DecisionProcesoPersistenceEntity, Integer>{

}
