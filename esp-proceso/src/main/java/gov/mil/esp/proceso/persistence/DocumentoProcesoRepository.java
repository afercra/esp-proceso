package gov.mil.esp.proceso.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import gov.mil.esp.proceso.persistence.entity.DocumentoProcesoPersistenceEntity;

public interface DocumentoProcesoRepository extends JpaRepository<DocumentoProcesoPersistenceEntity, Integer> {

	@Query(value = " SELECT coalesce(max(dc.id), 0) from espdb.documento_proceso dc ", nativeQuery = true)
	Integer getLastProcessId();

	@Query(value = " SELECT * FROM espdb.documento_proceso dp "
			+ "join proceso p "
			+ "on p.numero_proceso = dp.numero_proceso "
			+ "where dp.nombre_documento = :nombreDocumento "
			+ "AND "
			+ "p.nombre_proceso = :nombreProceso ", nativeQuery = true)
	DocumentoProcesoPersistenceEntity getDocumentoByNameAndProceso(@Param(value = "nombreProceso") String nombreProceso, @Param(value = "nombreDocumento") String nombreDocumento);

	@Query(value = " SELECT * FROM espdb.documento_proceso dp "
			+ "join proceso p "
			+ "on p.numero_proceso = dp.numero_proceso "
			+ "where dp.nombre_documento = :nombreDocumento "
			+ "AND "
			+ "p.numero_proceso = :numeroProceso ", nativeQuery = true)
	DocumentoProcesoPersistenceEntity getDocumentoByNameAndNumeroProceso(@Param(value = "numeroProceso") Integer numeroProceso, @Param(value = "nombreDocumento") String nombreDocumento);

}
