package gov.mil.esp.proceso.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import gov.mil.esp.proceso.persistence.entity.EstadoProcesoPersistenceEntity;
@Repository
public interface EstadoProcesoRepository extends JpaRepository<EstadoProcesoPersistenceEntity, String>{

}
