package gov.mil.esp.proceso.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import gov.mil.esp.proceso.persistence.entity.EvaluacionCriterioCalificacionProcesoDTOEntity;

public interface EvaluacionCriterioCalificacionProcesoFacadeRepository
		extends JpaRepository<EvaluacionCriterioCalificacionProcesoDTOEntity, Integer> {

	@Query(value = "SELECT criterio_evaluacion_proceso as criterio, aprobo FROM espdb.evaluacion_criterio_calificacion_proceso where numero_proceso = :proceso", nativeQuery = true)
	public List<EvaluacionCriterioCalificacionProcesoDTOEntity> getListaEvaluacionCriteriosCalificacion(
			@Param("proceso") Integer proceso);

}
