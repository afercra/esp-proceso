package gov.mil.esp.proceso.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import gov.mil.esp.proceso.persistence.entity.EvaluacionCriterioCalificacionProcesoPersistenceEntity;

public interface EvaluacionCriterioCalificacionProcesoRepository
		extends JpaRepository<EvaluacionCriterioCalificacionProcesoPersistenceEntity, Integer> {
		
	@Query(" SELECT coalesce(max(eccp.id), 0) from EvaluacionCriterioCalificacionProcesoPersistenceEntity eccp ")
	Integer getLastEvaluacionCriterioCalificacionProcesoRepositoryId();
		
	@Modifying
	@Query(nativeQuery = true, value = "INSERT INTO espdb.evaluacion_criterio_calificacion_proceso (numero_proceso, criterio_evaluacion_proceso, aprobo, id) VALUES(:proceso,:criterio, :aprobo, :id)")
	void crearEvaluacionCalificacionProceso(@Param("id") Integer id, @Param("proceso") Integer proceso, @Param("criterio") Integer criterioCalificacion, @Param("aprobo") boolean aprobo);

	
}
