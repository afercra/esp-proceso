package gov.mil.esp.proceso.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import gov.mil.esp.proceso.persistence.entity.GridAnalistaPersistenceEntity;
import gov.mil.esp.proceso.persistence.entity.GridAnalistaRequerimientosPersistenceEntity;
import gov.mil.esp.proceso.persistence.entity.GridRequerimientosTotalizadorPersitenceEntity;

@Repository
public interface GridAnalistaRepository extends JpaRepository<GridAnalistaPersistenceEntity, Integer> {

	@Query(nativeQuery = true, value = " SELECT " + "P.fecha_inicio AS FECHA_INSCRIPCION, " + "P.numero_proceso,  "
			+ " ASP.DOCUMENTO_IDENTIDAD AS NUMERO_DOCUMENTO , " + " ASP.TIPO_DOCUMENTO_IDENTIDAD AS TIPO_DOCUMENTO, "
			+ "P.NOMBRE_PROCESO AS NOMBRE_PROCESO "
			+ " ,CONCAT(ASP.PRIMER_NOMBRE,' ',ASP.SEGUNDO_NOMBRE,' ' ,ASP.PRIMER_APELLIDO, ' ', ASP.SEGUNDO_APELLIDO)  AS nombre "
			+ " ,CONCAT(ASP.PRIMER_NOMBRE,' ',ASP.SEGUNDO_NOMBRE,' ' ,ASP.PRIMER_APELLIDO, ' ', ASP.SEGUNDO_APELLIDO)  AS APELLIDOSYNOMBRES "
			+ " ,(select CASE WHEN (P.DECISION = '') IS TRUE then 'Pendiente' else DECIPRO.NOMBRE END  FROM espdb.decision_proceso DECIPRO WHERE P.DECISION = DECIPRO.CODIGO	) as DECISION "
			+ " ,(select CASE WHEN (DECIPRO.CODIGO = '') IS TRUE then null else DECIPRO.CODIGO END  FROM espdb.decision_proceso DECIPRO WHERE P.DECISION = DECIPRO.CODIGO) as ID_DECISION "
			+ " FROM espdb.proceso P" + " JOIN espdb.persona ASP ON "
			+ "ASP.DOCUMENTO_IDENTIDAD = P.DOCUMENTO_IDENTIDAD  "
			+ " WHERE P.FORMULARIO_DILIGENCIADO = :formularioDiligenciado AND P.ESTADO = 'ACT' ")
	List<GridAnalistaPersistenceEntity> listarAspirantes(
			@Param(value = "formularioDiligenciado") boolean formularioDiligenciado);

	@Query(nativeQuery = true, value = " SELECT " + "P.fecha_inicio AS FECHA_INSCRIPCION, " + "P.numero_proceso,  "
			+ " ASP.DOCUMENTO_IDENTIDAD AS NUMERO_DOCUMENTO , " + " ASP.TIPO_DOCUMENTO_IDENTIDAD AS TIPO_DOCUMENTO, "
			+ "P.NOMBRE_PROCESO AS NOMBRE_PROCESO "
			+ " ,CONCAT(ASP.PRIMER_NOMBRE,' ',ASP.SEGUNDO_NOMBRE,' ' ,ASP.PRIMER_APELLIDO, ' ', ASP.SEGUNDO_APELLIDO)  AS nombre "
			+ " ,CONCAT(ASP.PRIMER_NOMBRE,' ',ASP.SEGUNDO_NOMBRE,' ' ,ASP.PRIMER_APELLIDO, ' ', ASP.SEGUNDO_APELLIDO)  AS APELLIDOSYNOMBRES "
			+ " ,(select CASE WHEN (P.DECISION = '') IS  TRUE then 'Pendiente' else DECIPRO.NOMBRE END  FROM decision_proceso DECIPRO WHERE P.DECISION = DECIPRO.CODIGO	) as DECISION "
			+ " ,(select CASE WHEN (DECIPRO.CODIGO = '') IS TRUE then null else DECIPRO.CODIGO END  FROM decision_proceso DECIPRO WHERE P.DECISION = DECIPRO.CODIGO) as ID_DECISION "
			+ " FROM proceso P" + " JOIN persona ASP ON " + "ASP.DOCUMENTO_IDENTIDAD = P.DOCUMENTO_IDENTIDAD  "
			+ " WHERE P.DECISION = 'AVAL' AND P.ESTADO = 'ACT' AND P.REQUERIMIENTO IS NULL")
	List<GridAnalistaPersistenceEntity> listarAspirantesAvalados();

	@Query(nativeQuery = true, value = " SELECT " + " R.NOMBRE_REQUERIMIENTO AS numero_requerimiento,"
			+ " ER.NOMBRE AS estado_requerimiento " + " FROM ESPDB.REQUERIMIENTO R "
			+ " JOIN ESPDB.ESTADO_REQUERIMIENTO ER " + " ON R.ESTADO_REQUERIMIENTO = ER.ID"
			+ " WHERE ER.ID = 1 AND ER.ID != 3")
	List<GridAnalistaRequerimientosPersistenceEntity> listarRequerimientosAspirante();

	@Query(nativeQuery = true, value = " SELECT " 
	        + "CAST(P.fecha_inicio AS varchar) AS FECHA_INSCRIPCION, "
			+ "P.numero_proceso,  "
			+ " ASP.DOCUMENTO_IDENTIDAD AS NUMERO_DOCUMENTO , " + " ASP.TIPO_DOCUMENTO_IDENTIDAD AS TIPO_DOCUMENTO, "
			+ "P.NOMBRE_PROCESO AS NOMBRE_PROCESO "
			+ " ,CONCAT(ASP.PRIMER_NOMBRE,' ',ASP.SEGUNDO_NOMBRE,' ' ,ASP.PRIMER_APELLIDO, ' ', ASP.SEGUNDO_APELLIDO)  AS nombre "
			+ " ,CONCAT(ASP.PRIMER_NOMBRE,' ',ASP.SEGUNDO_NOMBRE,' ' ,ASP.PRIMER_APELLIDO, ' ', ASP.SEGUNDO_APELLIDO)  AS APELLIDOSYNOMBRES "
			+ " ,(select CASE WHEN (P.DECISION = '') IS TRUE then 'Pendiente' else DECIPRO.NOMBRE END  FROM ESPDB.decision_proceso DECIPRO WHERE P.DECISION = DECIPRO.CODIGO	) as DECISION "
			+ " ,(select CASE WHEN (DECIPRO.CODIGO = '') IS TRUE then null else DECIPRO.CODIGO END  FROM ESPDB.decision_proceso DECIPRO WHERE P.DECISION = DECIPRO.CODIGO) as ID_DECISION "
			+ " , p.entrevistador "
			+ "FROM ESPDB.proceso P" + " JOIN ESPDB.persona ASP ON "
			+ "ASP.DOCUMENTO_IDENTIDAD = P.DOCUMENTO_IDENTIDAD  "
			+ " WHERE P.ESTADO = 'ACT' AND P.REQUERIMIENTO = (SELECT R.ID FROM ESPDB.REQUERIMIENTO R WHERE R.NOMBRE_REQUERIMIENTO = :requerimiento)  ")
	List<GridRequerimientosTotalizadorPersitenceEntity> listarProcesosPorRequerimiento(@Param("requerimiento") String requerimiento);
	
	
	@Query(nativeQuery = true, value = " SELECT " + "P.fecha_inicio AS FECHA_INSCRIPCION, " + "P.numero_proceso,  "
			+ " ASP.DOCUMENTO_IDENTIDAD AS NUMERO_DOCUMENTO , " + " ASP.TIPO_DOCUMENTO_IDENTIDAD AS TIPO_DOCUMENTO, "
			+ "P.NOMBRE_PROCESO AS NOMBRE_PROCESO "
			+ " ,CONCAT(ASP.PRIMER_NOMBRE,' ',ASP.SEGUNDO_NOMBRE,' ' ,ASP.PRIMER_APELLIDO, ' ', ASP.SEGUNDO_APELLIDO)  AS nombre "
			+ " ,CONCAT(ASP.PRIMER_NOMBRE,' ',ASP.SEGUNDO_NOMBRE,' ' ,ASP.PRIMER_APELLIDO, ' ', ASP.SEGUNDO_APELLIDO)  AS APELLIDOSYNOMBRES "
			+ " ,(select CASE WHEN (P.DECISION = '') IS TRUE then 'Pendiente' else DECIPRO.NOMBRE END  FROM ESPDB.decision_proceso DECIPRO WHERE P.DECISION = DECIPRO.CODIGO	) as DECISION "
			+ " ,(select CASE WHEN (DECIPRO.CODIGO = '') IS TRUE then null else DECIPRO.CODIGO END  FROM ESPDB.decision_proceso DECIPRO WHERE P.DECISION = DECIPRO.CODIGO) as ID_DECISION "
			+ " FROM ESPDB.proceso P" + " JOIN ESPDB.persona ASP ON "
			+ "ASP.DOCUMENTO_IDENTIDAD = P.DOCUMENTO_IDENTIDAD  "
			+ " WHERE P.entrevistador = :entrevistador")
	List<GridAnalistaPersistenceEntity> getProcesosEntrevistador(@Param("entrevistador") String entrevistador);
	

}
