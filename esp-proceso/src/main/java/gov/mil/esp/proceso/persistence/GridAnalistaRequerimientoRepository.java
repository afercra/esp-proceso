package gov.mil.esp.proceso.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import gov.mil.esp.proceso.domain.dto.GridAnalistaRequerimientos;
import gov.mil.esp.proceso.persistence.entity.GridAnalistaPersistenceEntity;
import gov.mil.esp.proceso.persistence.entity.GridAnalistaRequerimientosPersistenceEntity;
@Repository
public interface GridAnalistaRequerimientoRepository extends JpaRepository<GridAnalistaRequerimientosPersistenceEntity,String>{

	@Query(nativeQuery=true, value=
			" SELECT "+	
			" R.NOMBRE_REQUERIMIENTO AS numero_requerimiento,"+
			" ER.NOMBRE AS estado_requerimiento "+
			" FROM ESPDB.REQUERIMIENTO R "+
			" JOIN ESPDB.ESTADO_REQUERIMIENTO ER "+
			" ON R.ESTADO_REQUERIMIENTO = ER.ID"+
			" WHERE ER.ID = 1 AND ER.ID != 3")
	List<GridAnalistaRequerimientosPersistenceEntity> listarRequerimientosAnalista();
	
	
	@Query(nativeQuery=true, value=
			" SELECT "+	
			" R.NOMBRE_REQUERIMIENTO AS numero_requerimiento,"+
			" ER.NOMBRE AS estado_requerimiento "+
			" FROM ESPDB.REQUERIMIENTO R "+
			" JOIN ESPDB.ESTADO_REQUERIMIENTO ER "+
			" ON R.ESTADO_REQUERIMIENTO = ER.ID"+
			" WHERE ER.ID = 2 ")
	List<GridAnalistaRequerimientosPersistenceEntity> listarRequerimientosTotalizador();
	

}
