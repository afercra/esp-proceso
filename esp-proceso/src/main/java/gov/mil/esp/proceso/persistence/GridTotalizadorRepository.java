package gov.mil.esp.proceso.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import gov.mil.esp.proceso.persistence.entity.GridRequerimientosTotalizadorPersitenceEntity;

@Repository
public interface GridTotalizadorRepository extends JpaRepository<GridRequerimientosTotalizadorPersitenceEntity, Integer> {

	
	@Query(nativeQuery = true, value = " SELECT " 
	        + "CAST(P.fecha_inicio AS varchar) AS FECHA_INSCRIPCION, "
			+ "P.numero_proceso,  "
			+ " ASP.DOCUMENTO_IDENTIDAD AS NUMERO_DOCUMENTO , " + " ASP.TIPO_DOCUMENTO_IDENTIDAD AS TIPO_DOCUMENTO, "
			+ "P.NOMBRE_PROCESO AS NOMBRE_PROCESO "
			+ " ,CONCAT(ASP.PRIMER_NOMBRE,' ',ASP.SEGUNDO_NOMBRE,' ' ,ASP.PRIMER_APELLIDO, ' ', ASP.SEGUNDO_APELLIDO)  AS nombre "
			+ " ,CONCAT(ASP.PRIMER_NOMBRE,' ',ASP.SEGUNDO_NOMBRE,' ' ,ASP.PRIMER_APELLIDO, ' ', ASP.SEGUNDO_APELLIDO)  AS APELLIDOSYNOMBRES "
			+ " ,(select CASE WHEN (P.DECISION = '') IS TRUE then 'Pendiente' else DECIPRO.NOMBRE END  FROM ESPDB.decision_proceso DECIPRO WHERE P.DECISION = DECIPRO.CODIGO	) as DECISION "
			+ " ,(select CASE WHEN (DECIPRO.CODIGO = '') IS TRUE then null else DECIPRO.CODIGO END  FROM ESPDB.decision_proceso DECIPRO WHERE P.DECISION = DECIPRO.CODIGO) as ID_DECISION "
			+ " , p.entrevistador "
			+ "FROM ESPDB.proceso P" + " JOIN ESPDB.persona ASP ON "
			+ "ASP.DOCUMENTO_IDENTIDAD = P.DOCUMENTO_IDENTIDAD  "
			+ " WHERE P.ESTADO = 'ACT' AND P.REQUERIMIENTO = (SELECT R.ID FROM ESPDB.REQUERIMIENTO R WHERE R.NOMBRE_REQUERIMIENTO = :requerimiento)  ")
	List<GridRequerimientosTotalizadorPersitenceEntity> listarProcesosPorRequerimiento(@Param("requerimiento") String requerimiento);
	
	

}
