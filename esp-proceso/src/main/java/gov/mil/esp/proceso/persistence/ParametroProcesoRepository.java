package gov.mil.esp.proceso.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import gov.mil.esp.proceso.persistence.entity.ParametroProcesoPersistenceEntity;

@Repository
public interface ParametroProcesoRepository extends JpaRepository<ParametroProcesoPersistenceEntity, String > {
	
}
