package gov.mil.esp.proceso.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import gov.mil.esp.proceso.persistence.entity.PersonaPersistenceEntity;

@Repository
public interface PersonaRepository extends JpaRepository<PersonaPersistenceEntity, String>{
	
	
	
	@Query(nativeQuery = true, value = "SELECT * FROM espdb.persona p where p.documento_identidad = :documentoIdentidad")
	PersonaPersistenceEntity selectByDocumentoIdentidad(@Param("documentoIdentidad")String documentoIdentidad);
}	