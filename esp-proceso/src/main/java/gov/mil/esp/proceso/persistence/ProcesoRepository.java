package gov.mil.esp.proceso.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import gov.mil.esp.proceso.persistence.entity.ProcesoPersistenceEntity;

@Repository
public interface ProcesoRepository extends JpaRepository<ProcesoPersistenceEntity, Integer>{
	
	
	@Query(" SELECT coalesce(max(p.numeroProceso), 0) from ProcesoPersistenceEntity p ")
	Integer getLastProcessId();
	
	
	@Modifying
	@Query(nativeQuery=true,value=" UPDATE proceso SET decision = :decision WHERE nombre_proceso IN (:procesos)")
	void actualizarDecisionProcesos(@Param("decision") String decision, @Param("procesos") List<String> procesos);
	
	@Modifying
	@Query(nativeQuery=true,value=" UPDATE espdb.proceso SET decision = :decision WHERE nombre_proceso = :proceso")
	void actualizarDecisionProceso(@Param("decision") String decision, @Param("proceso") String proceso);
	
	
	@Modifying
	@Query(nativeQuery=true,value=" UPDATE espdb.proceso SET decision = :decision, estado = 'FIN' WHERE numero_proceso = :proceso")
	void finalizarProceso(@Param("decision") String decision, @Param("proceso") Integer proceso);
	
	
	@Query(nativeQuery = true, value= "SELECT * FROM espdb.proceso WHERE nombre_proceso = :nombreProceso")
	ProcesoPersistenceEntity getProcesoPorNombre(@Param(value = "nombreProceso") String nombreProceso);
	

	@Query(nativeQuery = true, value= "SELECT COUNT(*) FROM espdb.proceso WHERE nombre_proceso = :nombreProceso")
	Integer contarProcesosPorNombre(@Param(value = "nombreProceso") String nombreProceso);
	
	@Query(nativeQuery = true, value= "SELECT COUNT(*) FROM espdb.proceso WHERE documento_identidad = :documentoIdentidad AND estado = 'ACT' ")
	Integer verificarProcesoActivoPersona(@Param(value = "documentoIdentidad") String documentoIdentidad);
	
	@Query(value = "SELECT nextval('espdb.consecutivo_proceso')", nativeQuery =
            true)
    Long getNextConsecutivoProceso();
	
	
	@Modifying
	@Query(value="UPDATE espdb.proceso SET entrevistador = :entrevistador  WHERE nombre_proceso = :nombreProceso", nativeQuery = true)
    public void asignarProcesoAEntrevistador(@Param("entrevistador") String entrevistador, @Param("nombreProceso") String nombreProceso);
	

	@Modifying
	@Query(value="UPDATE espdb.proceso SET entrevistador = null  WHERE nombre_proceso = :nombreProceso AND entrevistador = :entrevistador ", nativeQuery = true)
    public void desasignarProcesoAEntrevistador(@Param("entrevistador") String entrevistador, @Param("nombreProceso") String nombreProceso);
	
	
	//@Query(value = " ", nativeQuery = true)
	//public void getProcesosAsignadosAEntrevistador();
	
	@Query(value= " SELECT COUNT(*) "
				+" FROM espdb.proceso p "
				+" WHERE p.nombre_proceso = :nombreProceso"
			    +" AND p.estado ='ACT' AND p.entrevistador is null ", nativeQuery= true)
	public Integer verificarProcesoAsignado(@Param("nombreProceso") String nombreProceso);
	
	
	@Query(value= " SELECT COUNT(*) "
			+" FROM espdb.proceso p "
			+" WHERE p.nombre_proceso = :nombreProceso"
		    +" AND p.estado ='ACT' AND entrevistador = :entrevistador ", nativeQuery= true)
public Integer verificarProcesoAsignadoAEntrevistador(@Param("nombreProceso") String nombreProceso, @Param("entrevistador") String entrevistador);
	
	
	@Query(value=" SELECT COUNT(*) "
				 +" FROM espdb.usuario u "
				 +" JOIN espdb.usuario_roles ur "
				 +" ON ur.fk_usuario = u.documento_identidad "
			     +" WHERE u.documento_identidad = :documentoIdentidad "
			     +" AND ur.fk_rol = 'Entrevistador' "
			     +" AND u.activo = true ", nativeQuery= true)
	public Integer verificarExistenciaEntrevistador(@Param("documentoIdentidad") String documentoIdentidad);
	

}
