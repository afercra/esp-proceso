package gov.mil.esp.proceso.persistence;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import gov.mil.esp.proceso.persistence.entity.ProcesoPersistenceEntity;
import gov.mil.esp.proceso.persistence.entity.RequerimientoPersistenceEntity;

@Repository
public interface RequerimientoRepository extends JpaRepository<RequerimientoPersistenceEntity, Integer> {

	@Modifying
	@Query(nativeQuery = true, value = " INSERT INTO ESPDB.REQUERIMIENTO (id, fecha_emision, nombre_requerimiento, estado_requerimiento) "
			+ "VALUES (:id, :fecha_emision,:nombre_requerimiento, 1 )")
	void createRequerimiento(@Param("fecha_emision") Date fechaEmision,
			@Param("nombre_requerimiento") String nombreRequerimiento, @Param("id") Integer id);

	@Modifying
	@Query(value = " UPDATE ESPDB.PROCESO  SET REQUERIMIENTO =  :requerimiento WHERE  NOMBRE_PROCESO IN  (:procesos )", nativeQuery = true)
	void asociarProcesosARequerimiento(@Param("requerimiento") Integer requerimiento,
			@Param("procesos") List<String> procesos);
	@Modifying
	@Query(value = " DELETE FROM ESPDB.REQUERIMIENTO WHERE NOMBRE_REQUERIMIENTO = :nombreRequerimiento ", nativeQuery = true)
	
	void eliminarRequerimiento(@Param("nombreRequerimiento") String nombreRequerimiento);

	@Modifying
	@Query(value = " UPDATE ESPDB.PROCESO SET REQUERIMIENTO = null "
			+ "WHERE  NUMERO_PROCESO IN (SELECT PPP.NUMERO_PROCESO FROM ESPDB.PROCESO PPP WHERE PPP.REQUERIMIENTO = :requerimiento) ", nativeQuery = true)
	void desasociarProcesosDeRequerimiento(@Param("requerimiento") Integer requerimiento);

	@Modifying
	@Query(value = " SELECT * FROM ESPDB.PROCESO WHERE REQUERIMIENTO = :requerimiento ", nativeQuery = true)
	List<ProcesoPersistenceEntity> listProcesosRequerimiento(@Param("requerimiento") String requerimiento1);

	@Query(value = "SELECT nextval('espdb.requerimiento_sequence')", nativeQuery = true)
	Integer getNextRequerimientoId();

	@Query(value = "SELECT R.id FROM ESPDB.REQUERIMIENTO R WHERE R.NOMBRE_REQUERIMIENTO = :requerimiento ", nativeQuery = true)
	Integer getIdFromRequerimientoName(@Param("requerimiento") String requerimiento);
	
	@Modifying
	@Query(value = "UPDATE ESPDB.REQUERIMIENTO SET ESTADO_REQUERIMIENTO = :estadoRequerimiento WHERE NOMBRE_REQUERIMIENTO IN (:requerimientos)", nativeQuery = true)
	void cambiarEstadoRequerimiento(@Param("estadoRequerimiento") Integer estadoRequerimiento, @Param("requerimientos") List<String> requerimientos);

	@Query(value = "SELECT * FROM ESPDB.REQUERIMIENTO R WHERE R.NOMBRE_REQUERIMIENTO = :requerimiento ", nativeQuery = true)
	RequerimientoPersistenceEntity getRequerimientoFromNombre(@Param("requerimiento") String requerimiento);
	
}
