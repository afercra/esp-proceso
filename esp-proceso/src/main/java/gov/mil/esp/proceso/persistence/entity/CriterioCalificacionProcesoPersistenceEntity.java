package gov.mil.esp.proceso.persistence.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="criterio_calificacion_proceso", schema = "espdb")
public class CriterioCalificacionProcesoPersistenceEntity {
	
	@Id
	private Integer id;

	private String descripcion;
	
	private boolean excluyente;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "criterioEvaluacionProceso")
	private List<EvaluacionCriterioCalificacionProcesoPersistenceEntity> calificacionCriterioEvaluacionProcesoList;
	
}
