package gov.mil.esp.proceso.persistence.entity;


import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "decision_proceso")
public class DecisionProcesoPersistenceEntity {
	@Id
	@Column(name = "codigo")
	private String codigo;
	
	@Column(name = "nombre")
	private String nombre;

	@Column(name = "descripcion")
	private String descripcion;

	@OneToMany
	private List<ProcesoPersistenceEntity> proceso;
	
	

	public DecisionProcesoPersistenceEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DecisionProcesoPersistenceEntity(String codigo, String nombre, String descripcion) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<ProcesoPersistenceEntity> getProceso() {
		return proceso;
	}

	public void setProceso(List<ProcesoPersistenceEntity> proceso) {
		this.proceso = proceso;
	}

	

	
}
