package gov.mil.esp.proceso.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="documento_proceso", schema= "espdb")
public class DocumentoProcesoPersistenceEntity {
	
	@Id
	@Column(name = "id")
	@SequenceGenerator(name="documento_proceso_sequence", sequenceName = "espdb.documento_proceso_sequence", initialValue = 5, allocationSize = 1)
	private Integer idDocumento;
	
	Integer numeroProceso;
	
	private String nombreDocumento;
	
	byte [] documento;
	
	String observaciones;
	
	boolean tieneAntecedentes;
}
