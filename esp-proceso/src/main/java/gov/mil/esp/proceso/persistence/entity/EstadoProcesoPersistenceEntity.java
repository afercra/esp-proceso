package gov.mil.esp.proceso.persistence.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Table(name = "estado_proceso", schema="espdb")
@Entity
public class EstadoProcesoPersistenceEntity {

	@Id
	@Column(name = "codigo")
	private String id;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "descripcion")
	private String descripcion;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "estado")
	private List<ProcesoPersistenceEntity> proceso;

	public EstadoProcesoPersistenceEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EstadoProcesoPersistenceEntity(String id, String nombre, String descripcion,
			List<ProcesoPersistenceEntity> proceso) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.proceso = proceso;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public List<ProcesoPersistenceEntity> getProceso() {

		return proceso;
	}

	public void setProceso(List<ProcesoPersistenceEntity> proceso) {
		this.proceso = proceso;
	}

	



}
