package gov.mil.esp.proceso.persistence.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class EvaluacionCriterioCalificacionProcesoDTOEntity {

	@Id
	private Integer criterio;
	
	private boolean aprobo;

}
