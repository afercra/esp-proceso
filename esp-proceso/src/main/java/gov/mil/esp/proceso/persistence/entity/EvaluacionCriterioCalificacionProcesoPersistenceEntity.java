package gov.mil.esp.proceso.persistence.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "evaluacion_criterio_calificacion_proceso", schema = "espdb")
public class EvaluacionCriterioCalificacionProcesoPersistenceEntity {

	@Id
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "numero_proceso" )
	private ProcesoPersistenceEntity proceso;

	@ManyToOne	
	@JoinColumn(name= "criterio_evaluacion_proceso")
	private CriterioCalificacionProcesoPersistenceEntity criterioEvaluacionProceso;

	private boolean aprobo;
	

}
