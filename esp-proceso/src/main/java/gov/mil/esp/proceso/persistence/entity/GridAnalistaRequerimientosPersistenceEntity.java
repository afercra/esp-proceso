package gov.mil.esp.proceso.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class GridAnalistaRequerimientosPersistenceEntity {
	@Id
	@Column
	private String numeroRequerimiento;
	@Column
	private String estadoRequerimiento;
}
