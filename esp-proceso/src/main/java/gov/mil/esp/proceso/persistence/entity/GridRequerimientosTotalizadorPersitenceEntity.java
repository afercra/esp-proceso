package gov.mil.esp.proceso.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class GridRequerimientosTotalizadorPersitenceEntity {

	@Id
	@Column(name = "numero_proceso")
	private Integer numeroProceso;

	//private String fechaInicio;
	
	
	private String nombreProceso;

	private String apellidosYNombres;

	private String idDecision;

	private String decision;
	
	private String tipoDocumento;
	
	private String nombre;
	
	private String fechaInscripcion;
	
	private String numeroDocumento;
	
	private String entrevistador;
	
	
}
