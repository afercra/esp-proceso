package gov.mil.esp.proceso.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "parametro_proceso")
public class ParametroProcesoPersistenceEntity {

	@Id
	@Column(name = "nombre")
	private String nombre;

	@Column(name = "valor")
	private String valor;

	
	public ParametroProcesoPersistenceEntity() {
		
	}
	
	public ParametroProcesoPersistenceEntity(String nombre, String valor) {
		super();
		this.nombre = nombre;
		this.valor = valor;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	@Override
	public String toString() {
		return "ParametroProceso [nombre=" + nombre + ", valor=" + valor + ", getNombre()=" + getNombre()
				+ ", getValor()=" + getValor() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}
	
	

}
