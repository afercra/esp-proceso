package gov.mil.esp.proceso.persistence.entity;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;

@Entity
@Table(name = "persona")
public class PersonaPersistenceEntity {
	
	@Id
	private String documentoIdentidad;
	private String primerApellido;
	private String segundoApellido;
	private String primerNombre;
	private String segundoNombre;		
	private String tipoDocumentoIdentidad;
	
	private String numeroLibretaMilitar;
	private String claseLibretaMilitar;
	private String paisNacimiento;
	private String departamentoNacimiento;
	private String ciudadNacimiento;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private LocalDate fechaNacimiento;
	
	private String estadoCivil;
	private String correoElectronico;	
	private String division;
	private String brigada;
	private String batallon;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "persona")
	private List<ProcesoPersistenceEntity> proceso;

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "persona")
	@JoinColumn(name="documentoIdentidad")
	private UsuarioPersistenceEntity usuario;

	public PersonaPersistenceEntity() {

	}


	public PersonaPersistenceEntity(String primerApellido, String segundoApellido, String primerNombre, String segundoNombre,
			String tipoDocumento, String documentoIdentidad) {
		super();
		this.primerApellido = primerApellido;
		this.segundoApellido = segundoApellido;
		this.primerNombre = primerNombre;
		this.segundoNombre = segundoNombre;
		this.tipoDocumentoIdentidad = tipoDocumento;
		this.documentoIdentidad = documentoIdentidad;
	}


	public String getDocumentoIdentidad() {
		return documentoIdentidad;
	}


	public void setDocumentoIdentidad(String documentoIdentidad) {
		this.documentoIdentidad = documentoIdentidad;
	}


	public String getPrimerApellido() {
		return primerApellido;
	}


	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}


	public String getSegundoApellido() {
		return segundoApellido;
	}


	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}


	public String getPrimerNombre() {
		return primerNombre;
	}


	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}


	public String getSegundoNombre() {
		return segundoNombre;
	}


	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}


	public String getTipoDocumentoIdentidad() {
		return tipoDocumentoIdentidad;
	}


	public void setTipoDocumentoIdentidad(String tipoDocumentoIdentidad) {
		this.tipoDocumentoIdentidad = tipoDocumentoIdentidad;
	}


	public String getNumeroLibretaMilitar() {
		return numeroLibretaMilitar;
	}


	public void setNumeroLibretaMilitar(String numeroLibretaMilitar) {
		this.numeroLibretaMilitar = numeroLibretaMilitar;
	}


	public String getClaseLibretaMilitar() {
		return claseLibretaMilitar;
	}


	public void setClaseLibretaMilitar(String claseLibretaMilitar) {
		this.claseLibretaMilitar = claseLibretaMilitar;
	}


	public String getPaisNacimiento() {
		return paisNacimiento;
	}


	public void setPaisNacimiento(String paisNacimiento) {
		this.paisNacimiento = paisNacimiento;
	}


	public String getDepartamentoNacimiento() {
		return departamentoNacimiento;
	}


	public void setDepartamentoNacimiento(String departamentoNacimiento) {
		this.departamentoNacimiento = departamentoNacimiento;
	}


	public String getCiudadNacimiento() {
		return ciudadNacimiento;
	}


	public void setCiudadNacimiento(String ciudadNacimiento) {
		this.ciudadNacimiento = ciudadNacimiento;
	}


	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}


	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}


	public String getEstadoCivil() {
		return estadoCivil;
	}


	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}


	public String getCorreoElectronico() {
		return correoElectronico;
	}


	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}


	public String getDivision() {
		return division;
	}


	public void setDivision(String division) {
		this.division = division;
	}


	public String getBrigada() {
		return brigada;
	}


	public void setBrigada(String brigada) {
		this.brigada = brigada;
	}


	public String getBatallon() {
		return batallon;
	}


	public void setBatallon(String batallon) {
		this.batallon = batallon;
	}


	public List<ProcesoPersistenceEntity> getProceso() {
		return proceso;
	}


	public void setProceso(List<ProcesoPersistenceEntity> proceso) {
		this.proceso = proceso;
	}


	public UsuarioPersistenceEntity getUsuario() {
		return usuario;
	}


	public void setUsuario(UsuarioPersistenceEntity usuario) {
		this.usuario = usuario;
	}
	
	
}