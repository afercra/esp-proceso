package gov.mil.esp.proceso.persistence.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "proceso", schema = "espdb")
public class ProcesoPersistenceEntity {

	@Id
	@Column(name = "numero_proceso")
	@SequenceGenerator(name = "mySeqGen", sequenceName = "espdb.proceso_sequence", initialValue = 5, allocationSize = 1)
	private Integer numeroProceso;

	@Column(name = "fecha_inicio")
	private Date fechaInicio;

	@Column(name = "fecha_fin")
	private Date fechaFin;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "decision")
	private DecisionProcesoPersistenceEntity decision;

	// @OneToOne(fetch = FetchType.LAZY, optional = false)
	// @JoinColumn(name = "estado")
	@ManyToOne
	@JoinColumn(name = "estado")
	private EstadoProcesoPersistenceEntity estado;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "documento_identidad")
	PersonaPersistenceEntity persona;

	@OneToOne
	@JoinColumn(name = "entrevistador")
	private UsuarioPersistenceEntity entrevistador;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "proceso")
	private List<EvaluacionCriterioCalificacionProcesoPersistenceEntity> CalificacionCriterioProceso;

	private boolean formularioDiligenciado;

	private String nombreProceso;

	public ProcesoPersistenceEntity() {
		super();
		// TODO Auto-generated constructor stub

	}

	public ProcesoPersistenceEntity(int numeroProceso, EstadoProcesoPersistenceEntity estado,
			DecisionProcesoPersistenceEntity decisionProceso, Date fechaInicio, Date fechaFin, String nombreProceso) {
		super();
		this.numeroProceso = numeroProceso;
		this.estado = estado;
		this.decision = decisionProceso;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.nombreProceso = nombreProceso;
	}

	public ProcesoPersistenceEntity(Date fechaInicio, String nombreProceso) {
		super();
		this.fechaInicio = fechaInicio;
		this.nombreProceso = nombreProceso;
	}

	public Integer getNumeroProceso() {
		return numeroProceso;
	}

	public void setNumeroProceso(Integer numeroProceso) {
		this.numeroProceso = numeroProceso;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public DecisionProcesoPersistenceEntity getDecisionProceso() {
		return decision;
	}

	public void setDecisionProceso(DecisionProcesoPersistenceEntity decisionProceso) {
		this.decision = decisionProceso;
	}

	public EstadoProcesoPersistenceEntity getEstadoProceso() {
		return estado;
	}

	public void setEstadoProceso(EstadoProcesoPersistenceEntity estado) {
		this.estado = estado;
	}

	public PersonaPersistenceEntity getPersona() {
		return persona;
	}

	public void setPersona(PersonaPersistenceEntity persona) {
		this.persona = persona;
	}

	public EstadoProcesoPersistenceEntity getEstado() {
		return estado;
	}

	public void setEstado(EstadoProcesoPersistenceEntity estado) {
		this.estado = estado;
	}

	public UsuarioPersistenceEntity getEntrevistador() {
		return entrevistador;
	}

	public void setEntrevistador(UsuarioPersistenceEntity entrevistador) {
		this.entrevistador = entrevistador;
	}

	public String getNombreProceso() {
		return nombreProceso;
	}

	public void setNombreProceso(String nombreProceso) {
		this.nombreProceso = nombreProceso;
	}

	public DecisionProcesoPersistenceEntity getDecision() {
		return decision;
	}

	public void setDecision(DecisionProcesoPersistenceEntity decision) {
		this.decision = decision;
	}

	public boolean isFormularioDiligenciado() {
		return formularioDiligenciado;
	}

	public void setFormularioDiligenciado(boolean formularioDiligenciado) {
		this.formularioDiligenciado = formularioDiligenciado;
	}

	public List<EvaluacionCriterioCalificacionProcesoPersistenceEntity> getCalificacionCriterioProceso() {
		return CalificacionCriterioProceso;
	}

	public void setCalificacionCriterioProceso(
			List<EvaluacionCriterioCalificacionProcesoPersistenceEntity> calificacionCriterioProceso) {
		CalificacionCriterioProceso = calificacionCriterioProceso;
	}

	@Override
	public String toString() {
		return "ProcesoPersistenceEntity [numeroProceso=" + numeroProceso + ", fechaInicio=" + fechaInicio
				+ ", fechaFin=" + fechaFin + ", decision=" + decision + ", estado=" + estado + ", persona=" + persona
				+ ", entrevistador =" + entrevistador + ", formularioDiligenciado=" + formularioDiligenciado
				+ ", nombreProceso=" + nombreProceso + "]";
	}

}
