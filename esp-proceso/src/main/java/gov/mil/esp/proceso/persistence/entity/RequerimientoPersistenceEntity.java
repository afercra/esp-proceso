package gov.mil.esp.proceso.persistence.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="requerimiento")
public class RequerimientoPersistenceEntity {
	
	@Id
	@SequenceGenerator(name="mySeqGen", sequenceName = "espdb.requerimiento_sequence", initialValue = 5, allocationSize = 1)
	@Column
	Integer id;
	@Column
	Date fechaEmision;
	@Column
	String nombreRequerimiento;
	@Column
	Integer estadoRequerimiento;
	
	public RequerimientoPersistenceEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RequerimientoPersistenceEntity( Date fechaEmision, String nombreRequerimiento) {
		super();
		this.fechaEmision = fechaEmision;
		this.nombreRequerimiento = nombreRequerimiento;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public String getNombreRequerimiento() {
		return nombreRequerimiento;
	}

	public void setNombreRequerimiento(String nombreRequerimiento) {
		this.nombreRequerimiento = nombreRequerimiento;
	}
	
	public Integer getEstadoRequerimiento() {
		return estadoRequerimiento;
	}

	public void setEstadoRequerimiento(Integer estadoRequerimiento) {
		this.estadoRequerimiento = estadoRequerimiento;
	}

	@Override
	public String toString() {
		return "Requerimiento [id=" + id + ", fechaEmision=" + fechaEmision + ", nombreRequerimiento="
				+ nombreRequerimiento + "]";
	}
	
	
	
	
}
