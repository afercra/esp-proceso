package gov.mil.esp.proceso.persistence.entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Table(name="usuario")
public class UsuarioPersistenceEntity{
	
	@Id
	@Column(name="documento_identidad")
	private String documentoIdentidad;	
	
	@Column(name="password")
	private String password;

	
	@OneToOne
	@JoinColumn(name="documento_identidad")
	private PersonaPersistenceEntity persona;
	
	@Column(name="activo")
	boolean activo;
	
	
	
	public UsuarioPersistenceEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UsuarioPersistenceEntity(String documentoIdentidad, String password, boolean activo) {
		super();
		this.documentoIdentidad = documentoIdentidad;
		this.password = password;
		this.activo = activo;
	}

	public String getDocumentoIdentidad() {
		return documentoIdentidad;
	}

	public void setDocumentoIdentidad(String documentoIdentidad) {
		this.documentoIdentidad = documentoIdentidad;
	}

	public PersonaPersistenceEntity getPersona() {
		return persona;
	}

	public void setPersona(PersonaPersistenceEntity persona) {
		this.persona = persona;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	
	@Override
	public String toString() {
		return "Usuario [documentoIdentidad=" + documentoIdentidad + ", password=" + password + ", activo=" + activo
				+ "]";
	}
	
	

}
