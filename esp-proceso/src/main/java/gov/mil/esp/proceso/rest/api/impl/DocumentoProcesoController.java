package gov.mil.esp.proceso.rest.api.impl;

import javax.validation.Valid;

import org.postgresql.util.PSQLException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gov.mil.esp.proceso.domain.business.service.impl.DocumentoProcesoService;
import gov.mil.esp.proceso.domain.dto.CrearDocumentoDTO;
import gov.mil.esp.proceso.domain.dto.Request;
import gov.mil.esp.proceso.domain.exception.DocumentoNoexisteException;
import gov.mil.esp.proceso.domain.exception.ProcesoNoExisteException;


@RestController
@RequestMapping(value = "/documento")
@CrossOrigin(origins = "*")
public class DocumentoProcesoController {

	private DocumentoProcesoService documentoProcesoService;
	
	public DocumentoProcesoController(DocumentoProcesoService documentoProcesoService) {
		this.documentoProcesoService = documentoProcesoService;
	}
	
	@PostMapping
	public ResponseEntity<?> uploadDocument(@Valid @RequestBody Request<CrearDocumentoDTO> crearDocumentoRequest) throws PSQLException, ProcesoNoExisteException { 
		documentoProcesoService.uploadDocument(crearDocumentoRequest);	
		return null;
	}
	
	@GetMapping(value="/{nombreProceso}/{nombreDocumento}")
    public ResponseEntity<?> getDocument(@PathVariable(name = "nombreProceso") String nombreProceso, @PathVariable(name = "nombreDocumento") String nombreDocumento) throws DocumentoNoexisteException { 		
		return ResponseEntity.status(HttpStatus.OK).body(documentoProcesoService.getDocumento(nombreProceso, nombreDocumento));
	}
	
	
	@GetMapping
    public ResponseEntity<?> getDocumentosProceso(@PathVariable(name = "nombreProceso") String nombreProceso, @PathVariable(name = "nombreDocumento") String nombreDocumento) throws DocumentoNoexisteException { 		
		return ResponseEntity.status(HttpStatus.OK).body(documentoProcesoService.getDocumento(nombreProceso, nombreDocumento));
	}
    
    
	
}
