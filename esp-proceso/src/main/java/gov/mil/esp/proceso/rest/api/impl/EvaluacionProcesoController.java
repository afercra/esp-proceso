package gov.mil.esp.proceso.rest.api.impl;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gov.mil.esp.proceso.domain.business.service.EvaluacionProcesoService;
import gov.mil.esp.proceso.domain.business.service.impl.EvaluacionProcesoServiceImpl;
import gov.mil.esp.proceso.domain.dto.EvaluacionCriterioCalificacionProcesoDTO;
import gov.mil.esp.proceso.domain.dto.Request;

@RestController
@RequestMapping(value = "/evaluacion/**")
@CrossOrigin(origins = "*")
public class EvaluacionProcesoController {

	private EvaluacionProcesoService evaluacionProcesoService;

	@Autowired
	public EvaluacionProcesoController(EvaluacionProcesoServiceImpl evaluacionProcesoServiceImpl) {
		this.evaluacionProcesoService = evaluacionProcesoServiceImpl;
	}

	public ResponseEntity<?> getCriteriosCalificacion() {
		return ResponseEntity.status(HttpStatus.OK).body(null);
	}

	@GetMapping("/{numeroProceso}")
	public ResponseEntity<?> getEvaluacionCriteriosCalificacionProceso(@PathVariable("numeroProceso") Integer numeroProceso) {
		System.out.println("On controller ------------------------->");
		System.out.println(numeroProceso);
		return ResponseEntity.status(HttpStatus.OK)
				.body(evaluacionProcesoService.getEvaluacionCriteriosCalificacionProceso(numeroProceso));
	}

	@PostMapping
	public ResponseEntity<?> saveEvaluacionCriteriosCalificacionProceso(
			@RequestBody @Valid Request<EvaluacionCriterioCalificacionProcesoDTO> request) {
		return ResponseEntity.status(HttpStatus.OK)
				.body(evaluacionProcesoService.saveEvaluacionCriterios(request.getMensaje()));
	}

}
