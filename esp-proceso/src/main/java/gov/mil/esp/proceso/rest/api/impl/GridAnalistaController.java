package gov.mil.esp.proceso.rest.api.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gov.mil.esp.proceso.domain.Response;
import gov.mil.esp.proceso.domain.business.service.impl.GridAnalistaServiceImpl;
import gov.mil.esp.proceso.domain.dto.GridAnalista;
import gov.mil.esp.proceso.domain.dto.GridAnalistaRequerimientos;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/analista")
public class GridAnalistaController {

	GridAnalistaServiceImpl gridAnalistaService;

	@Autowired
	public GridAnalistaController(GridAnalistaServiceImpl gridAnalistaService) {
		this.gridAnalistaService = gridAnalistaService;
	}

	@GetMapping(value = "/list/{formularioDiligenciado}")
	public Response<?> listarAspirantes(@PathVariable boolean formularioDiligenciado) {
		System.out.println("in listarAspirante ---------->");
		Response<List<GridAnalista>> response;
		response = gridAnalistaService.listarProcesos(formularioDiligenciado);
		return response;
	}
	

	@GetMapping(value = "/avalados")
	public Response<?> listarAspirantesAvalados() {
		Response<List<GridAnalista>> response;
		response = gridAnalistaService.listarAvalados();
		return response;
	}

	@GetMapping(value = "/requerimientos")
	public Response<?> listarRequerimientosAnalista() {
		Response<List<GridAnalistaRequerimientos>> response;
		response = gridAnalistaService.listarRequerimientos();
		return response;
	}
	

}
