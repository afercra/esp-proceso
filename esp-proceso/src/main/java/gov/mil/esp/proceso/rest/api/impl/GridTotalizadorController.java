package gov.mil.esp.proceso.rest.api.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gov.mil.esp.proceso.domain.Response;
import gov.mil.esp.proceso.domain.business.service.impl.GridAnalistaServiceImpl;
import gov.mil.esp.proceso.domain.dto.GridAnalistaRequerimientos;
import gov.mil.esp.proceso.domain.exception.EntrevistadorNoExisteException;
import gov.mil.esp.proceso.domain.exception.ProcesoNoAsignableException;
import gov.mil.esp.proceso.domain.exception.ProcesoNoAsignadoException;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/totalizador")
public class GridTotalizadorController {

	private GridAnalistaServiceImpl gridAnalistaService;
	
	@Autowired
	public GridTotalizadorController(GridAnalistaServiceImpl gridAnalistaService) {
		this.gridAnalistaService = gridAnalistaService;
	}
	
	@GetMapping(value = "/requerimientos")
	public Response<?> listarRequerimientosTotalizador() {
		Response<List<GridAnalistaRequerimientos>> response;
		response = gridAnalistaService.listarRequerimientosTotalizador();
		return response;
	}
	
	
	@GetMapping(value="/procesosEntrevistador/{entrevistador}")
	public ResponseEntity<?> getProcesosEntrevistador(@PathVariable("entrevistador") String entrevistador) throws ProcesoNoAsignableException, EntrevistadorNoExisteException, ProcesoNoAsignadoException {	
		return ResponseEntity.status(HttpStatus.OK).body(gridAnalistaService.getProcesosEntrevistador(entrevistador));
	}
	
	
	
	
	
}
