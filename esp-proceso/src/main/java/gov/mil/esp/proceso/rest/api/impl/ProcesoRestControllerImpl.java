package gov.mil.esp.proceso.rest.api.impl;

import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gov.mil.esp.proceso.domain.Response;
import gov.mil.esp.proceso.domain.business.service.ProcesoService;
import gov.mil.esp.proceso.domain.business.service.impl.ProcesoServiceImpl;
import gov.mil.esp.proceso.domain.dto.AsignarProcesoAEntrevistadorDTO;
import gov.mil.esp.proceso.domain.dto.CambiarDecisionProcesoDTO;
import gov.mil.esp.proceso.domain.dto.CreacionProcesoDTO;
import gov.mil.esp.proceso.domain.dto.Request;
import gov.mil.esp.proceso.domain.exception.EntrevistadorNoExisteException;
import gov.mil.esp.proceso.domain.exception.ProcesoEnCursoException;
import gov.mil.esp.proceso.domain.exception.ProcesoNoAsignableException;
import gov.mil.esp.proceso.domain.exception.ProcesoNoAsignadoException;
import gov.mil.esp.proceso.model.Proceso;

@SuppressWarnings("hiding")
@RestController
@RequestMapping(value = "/proceso/**")
@CrossOrigin(origins = "*")
public class ProcesoRestControllerImpl {

	private ProcesoService procesoService;

	@Autowired
	public ProcesoRestControllerImpl(ProcesoServiceImpl procesoService) {
		this.procesoService = procesoService;
	}

	@PostMapping(value = "/async")
	public ResponseEntity IniciarProcesoAPartirDeDocumento(
			@RequestBody Request<CreacionProcesoDTO> numeroDocumento) {
		Response<Proceso> response;
		response = procesoService
				.enviarIdentificacionAColaInicioProceso(numeroDocumento.getMensaje().getDocumentoIdentidad());
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@PutMapping
	public ResponseEntity<?> cambiarDecisionProceso(
			@RequestBody Request<CambiarDecisionProcesoDTO> cambiarEstadoProcesoRequest) {
		return ResponseEntity.status(HttpStatus.OK)
				.body(procesoService.cambiarDecisionProceso(cambiarEstadoProcesoRequest));
	}

	@PostMapping
	public ResponseEntity<?> crearProceso(@RequestBody Request<CreacionProcesoDTO> request)
			throws NoSuchElementException, ProcesoEnCursoException {
		return ResponseEntity.status(HttpStatus.OK)
				.body(procesoService.crearProceso(request.getMensaje().getDocumentoIdentidad()));
	}

	@PutMapping(value = "/asignar/entrevista")
	public ResponseEntity<?> asignarProcesoAEntrevistador(
			@Valid @RequestBody Request<AsignarProcesoAEntrevistadorDTO> request)
			throws ProcesoNoAsignableException, EntrevistadorNoExisteException {
		return ResponseEntity.status(HttpStatus.OK).body(procesoService.asignarProcesoAEntrevistador(request));
	}

	@PutMapping(value = "/desasignar/entrevista")
	public ResponseEntity<?> desAsignarProcesoAEntrevistador(
			@Valid @RequestBody Request<AsignarProcesoAEntrevistadorDTO> request)
			throws ProcesoNoAsignableException, EntrevistadorNoExisteException, ProcesoNoAsignadoException {
		return ResponseEntity.status(HttpStatus.OK).body(procesoService.desAsignarProcesoAEntrevistador(request));
	}



}
