package gov.mil.esp.proceso.rest.api.impl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gov.mil.esp.proceso.domain.business.service.impl.GridAnalistaServiceImpl;
import gov.mil.esp.proceso.domain.business.service.impl.RequerimientoServiceImpl;
import gov.mil.esp.proceso.domain.dto.CambiarEstadoRequerimientoDTO;
import gov.mil.esp.proceso.domain.dto.CreacionRequerimientoDTO;
import gov.mil.esp.proceso.domain.dto.Request;

@RestController
@RequestMapping(value = "/requerimiento")
@CrossOrigin(origins = "*")
public class RequerimientoRestController {
	
	private RequerimientoServiceImpl requerimientoService;
	private GridAnalistaServiceImpl gridAnalistaService;
	
	@Autowired
	public RequerimientoRestController(RequerimientoServiceImpl requerimientoService, GridAnalistaServiceImpl gridAnalistaService) {
		this.requerimientoService = requerimientoService;
		this.gridAnalistaService =gridAnalistaService;
	}
	
	@GetMapping(value = "/procesos/{requerimiento}")
	public ResponseEntity<?>  listarProcesosDeRequerimiento (@PathVariable String requerimiento) {
		return ResponseEntity.status(HttpStatus.OK).body(gridAnalistaService.listarProcesosDeRequerimiento(requerimiento));
	}
	
	
	@PostMapping	
	public ResponseEntity<?> crearRequerimiento(@RequestBody @Valid Request<List<CreacionRequerimientoDTO>> procesosList) {
		return ResponseEntity.status(HttpStatus.OK).body(requerimientoService.crearRequerimiento(procesosList));
	}
	
	
	@DeleteMapping(value = "/{requerimiento}")
	public ResponseEntity<?> eliminarRequerimiento(@PathVariable String requerimiento) {
		return ResponseEntity.status(HttpStatus.OK).body(requerimientoService.eliminarRequerimiento(requerimiento));
	}
	

	@PutMapping
	public ResponseEntity<?> cambiarEstadoRequerimiento(@RequestBody @Valid Request<CambiarEstadoRequerimientoDTO> requerimientosRequest){		
		return ResponseEntity.status(HttpStatus.OK).body(requerimientoService.cambiarEstadoRequerimiento(requerimientosRequest));
	}
	
	
	

}
